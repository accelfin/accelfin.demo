package com.accelfin.server.demo.fx.model.orders.execution;

import com.accelfin.server.SessionToken;
import com.accelfin.server.demo.common.model.Side;
import com.accelfin.server.demo.fx.dtos.OrderBlotterUpdateResponseDto;
import com.accelfin.server.demo.fx.dtos.OrderDto;
import com.accelfin.server.demo.fx.dtos.OrderStatusDto;
import com.accelfin.server.demo.fx.model.orders.blotter.OrderStatus;
import com.accelfin.server.demo.fx.model.orders.blotter.SimpleOrder;
import com.accelfin.server.demo.fx.model.infrastructure.FxModelTestBase;
import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertEquals;

public class OrderMatcherTests extends FxModelTestBase {

    private SessionToken _sessionToken;

    @Override
    public void setUp() throws Exception {
        super.setUp();
        initialiseModel();
        _sessionToken = TestUsers
                .keith()
                .logon()
                .getSessionToken();
        clearWebOutboundMessages();
    }

    @Test
    public void orderStatusIsActiveUponBooking() throws Exception {
        Scenarios.executeSimpleOrder(_sessionToken, "EURUSD", Side.Buy, 1.5);
        assertEquals(OrderStatus.ACTIVE, getOrder(0).getStatus());
    }

    @Test
    public void buyOrderIsExecutedWhenRateEqualsExecuteAtRate() throws Exception {
        Scenarios.executeSimpleOrder(_sessionToken, "EURUSD", Side.Buy, 1.5);
        Scenarios.publishPrice("EURUSD",  BigDecimal.valueOf(1.4), BigDecimal.valueOf(1.5));
        assertEquals(OrderStatus.EXECUTED, getOrder(0).getStatus());
    }

    @Test
    public void buyOrderIsExecutedWhenRateBelowExecuteAtRate() throws Exception {
        Scenarios.executeSimpleOrder(_sessionToken, "EURUSD", Side.Buy, 1.5);
        Scenarios.publishPrice("EURUSD",  BigDecimal.valueOf(1.4), BigDecimal.valueOf(1.49));
        assertEquals(OrderStatus.EXECUTED, getOrder(0).getStatus());
    }

    @Test
    public void buyOrderIsNotExecutedWhenRateBelowExecuteAtRate() throws Exception {
        Scenarios.executeSimpleOrder(_sessionToken, "EURUSD", Side.Buy, 1.5);
        Scenarios.publishPrice("EURUSD",  BigDecimal.valueOf(1.4), BigDecimal.valueOf(1.51));
        assertEquals(OrderStatus.ACTIVE, getOrder(0).getStatus());
    }

    @Test
    public void allBuyOrderAreExecutedWhenRateBelowExecuteAtRate() throws Exception {
        Scenarios.executeSimpleOrder(_sessionToken, "EURUSD", Side.Buy, 1.4);
        Scenarios.executeSimpleOrder(_sessionToken, "EURUSD", Side.Buy, 1.5);
        Scenarios.executeSimpleOrder(_sessionToken, "EURUSD", Side.Buy, 1.6);
        assertEquals(3, SimpleOrderTradeRepository.getAllTrades().size());
        Scenarios.publishPrice("EURUSD",  BigDecimal.valueOf(1.41), BigDecimal.valueOf(1.42));
        assertEquals(OrderStatus.ACTIVE, getOrder(0).getStatus());
        assertEquals(OrderStatus.EXECUTED, getOrder(1).getStatus());
        assertEquals(OrderStatus.EXECUTED, getOrder(2).getStatus());
    }

    @Test
    public void sellOrderIsExecutedWhenRateEqualsExecuteAtRate() throws Exception {
        Scenarios.executeSimpleOrder(_sessionToken, "EURUSD", Side.Sell, 1.5);
        Scenarios.publishPrice("EURUSD",  BigDecimal.valueOf(1.5), BigDecimal.valueOf(1.6));
        assertEquals(OrderStatus.EXECUTED, getOrder(0).getStatus());
    }

    @Test
    public void sellOrderIsExecutedWhenRateAboveExecuteAtRate() throws Exception {
        Scenarios.executeSimpleOrder(_sessionToken, "EURUSD", Side.Sell, 1.5);
        Scenarios.publishPrice("EURUSD",  BigDecimal.valueOf(1.6), BigDecimal.valueOf(1.7));
        assertEquals(OrderStatus.EXECUTED, getOrder(0).getStatus());
    }

    @Test
    public void sellOrderIsNotExecutedWhenRateBelowExecuteAtRate() throws Exception {
        Scenarios.executeSimpleOrder(_sessionToken, "EURUSD", Side.Sell, 1.5);
        Scenarios.publishPrice("EURUSD",  BigDecimal.valueOf(1.4), BigDecimal.valueOf(1.51));
        assertEquals(OrderStatus.ACTIVE, getOrder(0).getStatus());
    }

    @Test
    public void allSellOrderAreExecutedWhenRateAboveExecuteAtRate() throws Exception {
        Scenarios.executeSimpleOrder(_sessionToken, "EURUSD", Side.Sell, 1.4);
        Scenarios.executeSimpleOrder(_sessionToken, "EURUSD", Side.Sell, 1.5);
        Scenarios.executeSimpleOrder(_sessionToken, "EURUSD", Side.Sell, 1.6);
        assertEquals(3, SimpleOrderTradeRepository.getAllTrades().size());
        Scenarios.publishPrice("EURUSD",  BigDecimal.valueOf(1.55), BigDecimal.valueOf(1.56));
        assertEquals(OrderStatus.EXECUTED, getOrder(0).getStatus());
        assertEquals(OrderStatus.EXECUTED, getOrder(1).getStatus());
        assertEquals(OrderStatus.ACTIVE, getOrder(2).getStatus());
    }

    @Test
    public void orderIsNotExecutedTwice() throws Exception {
        Scenarios.executeSimpleOrder(_sessionToken, "EURUSD", Side.Sell, 1.5);
        Scenarios.publishPrice("EURUSD",  BigDecimal.valueOf(1.5), BigDecimal.valueOf(1.6));
        SimpleOrder order = getOrder(0);
        assertEquals(OrderStatus.EXECUTED, order.getStatus());
        assertTrue(BigDecimal.valueOf(1.5).equals(order.getExecutedRate()));
        Scenarios.publishPrice("EURUSD",  BigDecimal.valueOf(1.6), BigDecimal.valueOf(1.7));
        BigDecimal executedRate = order.getExecutedRate();
        assertFalse(BigDecimal.valueOf(1.6).equals(executedRate));
    }

    @Test
    public void executedOrderStatusUpdateSentToClient() throws Exception {
        Scenarios.subscribeToOrderBlotter(_sessionToken);
        Scenarios.executeSimpleOrder(_sessionToken, "EURUSD", Side.Sell, 1.5);
        clearWebOutboundMessages();
        Scenarios.publishPrice("EURUSD",  BigDecimal.valueOf(1.6), BigDecimal.valueOf(1.7));
        OrderBlotterUpdateResponseDto responseDto = getWebOutboundMessagesPayload(0);
        OrderDto orderDto = responseDto.getOrders(0);
        assertEquals(OrderStatusDto.EXECUTED, orderDto.getStatus());
    }

    private SimpleOrder getOrder(int index) {
        return SimpleOrderTradeRepository.getAllTrades().get(index);
    }
}