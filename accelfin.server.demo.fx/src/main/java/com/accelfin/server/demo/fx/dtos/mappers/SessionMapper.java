package com.accelfin.server.demo.fx.dtos.mappers;

import com.accelfin.server.Clock;
import com.accelfin.server.messaging.mappers.CommonTypesMapper;
import com.accelfin.server.messaging.MessageEnvelope;
import com.accelfin.server.messaging.events.InboundMessageEvent;
import com.accelfin.server.demo.fx.dtos.*;
import com.accelfin.server.demo.fx.model.User;
import com.accelfin.server.demo.fx.model.events.operationEvents.ClientHeartbeatOperationEvent;
import com.accelfin.server.demo.fx.model.events.operationEvents.LoginOperationEvent;
import com.google.inject.Inject;

import java.util.Arrays;

import static com.accelfin.server.messaging.mappers.CommonTypesMapper.mapInstantFromDto;

public class SessionMapper {

    private Clock _clock;

    @Inject
    public SessionMapper(Clock clock) {
        _clock = clock;
    }

    public InboundMessageEvent mapLoginOperationEvent(MessageEnvelope envelope) {
        LoginRequestDto dto = envelope.getPayload();
        return new LoginOperationEvent(
                dto.getUserName(),
                dto.getPassword(),
                dto.getClientSessionId()
        );
    }

    public LoginResponseDto mapFailedLoginReponseDto() {
        return LoginResponseDto.newBuilder()
                .setIsSuccess(false)
                .build();
    }

    public LoginResponseDto mapLoginSuccessReponseDto(String sessionId, User user) {
        return LoginResponseDto.newBuilder()
                .setSessionId(sessionId)
                .setIsSuccess(true)
                .setUser(mapUser(user))
                .build();
    }

    private UserDto mapUser(User user) {
        return UserDto.newBuilder()
                .setUserName(user.getUserName())
                .setFirstName(user.getFirstName())
                .setLastName(user.getLastName())
                .addAllPermissions(Arrays.asList(user.getPermissions()))
                .build();
    }

    public InboundMessageEvent mapClientHeartbeatOperationEvent(MessageEnvelope envelope) {
        ClientHeartbeatRequestDto dto = envelope.getPayload();
        return new ClientHeartbeatOperationEvent(
                mapInstantFromDto(dto.getTimeSent())
        );
    }

    public ClientHeartbeatResponseDto mapClientHeartbeatResponse() {
        return ClientHeartbeatResponseDto.newBuilder()
                .setTimeSent(CommonTypesMapper.mapInstantToDto(_clock.now()))
                .build();
    }
}
