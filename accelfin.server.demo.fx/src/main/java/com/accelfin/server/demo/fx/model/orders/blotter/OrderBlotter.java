package com.accelfin.server.demo.fx.model.orders.blotter;

import com.accelfin.server.demo.common.messaging.ConnectionBroker;
import com.accelfin.server.SessionToken;
import com.accelfin.server.demo.FxOperationNameConst;
import com.accelfin.server.demo.fx.dtos.mappers.BlotterMapper;
import com.accelfin.server.demo.fx.model.FxService;
import com.accelfin.server.demo.fx.model.common.BlotterBase;
import com.accelfin.server.demo.fx.model.common.BlotterUpdateType;
import com.accelfin.server.demo.fx.model.common.TradeRepository;
import com.accelfin.server.demo.fx.model.events.OrdersFilledEvent;
import com.accelfin.server.demo.fx.model.events.SimpleOrderExecutedEvent;
import com.accelfin.server.demo.fx.model.events.operationEvents.OrderBlotterSubscribeOperationEvent;
import com.esp.EventContext;
import com.esp.ObserveEvent;
import com.esp.Router;
import com.google.inject.Inject;

public class OrderBlotter extends BlotterBase<SimpleOrder> {
    private ConnectionBroker _connectionBroker;

    @Inject
    public OrderBlotter(
            Router<FxService> router,
            ConnectionBroker connectionBroker,
            TradeRepository<SimpleOrder> tradeRepository,
            BlotterMapper<SimpleOrder> blotterMapper
    ) {
        super(router, connectionBroker, tradeRepository, blotterMapper);
        _connectionBroker = connectionBroker;
    }

    @Override
    protected void setOperationStatus(){
        _connectionBroker.setOperationStatus(FxOperationNameConst.OrderBlotterUpdate, true);
        _connectionBroker.setOperationStatus(FxOperationNameConst.OrderBlotterSubscribe, true);
    }

    @Override
    protected String getBlotterOperationName() {
        return FxOperationNameConst.OrderBlotterUpdate;
    }

    @ObserveEvent(eventClass = OrderBlotterSubscribeOperationEvent.class)
    public void onBlotterSubscribeOperationEvent(OrderBlotterSubscribeOperationEvent event) {
        processSubscriptionEvent(event);
    }

    @ObserveEvent(eventClass = SimpleOrderExecutedEvent.class)
    public void onSpotTradeExecutionEvent(SimpleOrderExecutedEvent event, EventContext eventContext) {
        onTradeExecutionEvent(event, eventContext);
    }

    @ObserveEvent(eventClass = OrdersFilledEvent.class)
    public void onOrdersFilledEvent(OrdersFilledEvent event) {
        SessionToken[] subscribers = getBlotterSubscriptions().toArray(new SessionToken[getBlotterSubscriptions().size()]);
        dispatchUpdate(
                BlotterUpdateType.Normal,
                event.getOrders(),
                subscribers
        );
    }
}
