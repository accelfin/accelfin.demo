package com.accelfin.server.demo.fx.model.session;

import com.accelfin.server.Clock;
import com.accelfin.server.SessionToken;
import com.accelfin.server.demo.fx.model.User;

import java.time.Duration;
import java.time.Instant;

public class Session {
    private static final int SESSION_TIMEOUT_MS = 15000; // kill if no update in 15sec
    private User _user;
    private SessionToken _sessionToken;
    private Clock _clock;
    private Instant _lastSeenTime;
    private boolean _hasExpired = false;

    public Session(User user, SessionToken sessionToken, Clock clock) {
        _user = user;
        _sessionToken = sessionToken;
        _clock = clock;
        _lastSeenTime = _clock.now();
    }

    public User getUser() {
        return _user;
    }

    public SessionToken getSessionToken() {
        return _sessionToken;
    }

    public Instant getLastSeenTime() {
        return _lastSeenTime;
    }

    public void updateLastSeenTime() {
        _lastSeenTime = _clock.now();
    }

    public boolean getHasExpired() {
        return _hasExpired;
    }

    public void updateHasExpired() {
       _hasExpired = _lastSeenTime
                .plus(Duration.ofMillis(SESSION_TIMEOUT_MS))
                .isBefore(_clock.now());
    }
}
