package com.accelfin.server.demo.fx.model.orders.blotter;

import com.accelfin.server.demo.common.model.CurrencyPair;
import com.accelfin.server.demo.common.model.Side;
import com.accelfin.server.demo.fx.model.User;
import com.accelfin.server.demo.fx.model.common.TradeBase;

import java.math.BigDecimal;
import java.time.Instant;

public class SimpleOrder extends TradeBase {
    private User _user;
    private Side _side;
    private OrderType _type;
    private BigDecimal _notional;
    private BigDecimal _submittedRate;
    private Instant _executedAt;
    private OrderStatus _status;
    private BigDecimal _executedRate;

    public SimpleOrder(
            String tradeId,
            CurrencyPair currencyPair,
            User user,
            Side side,
            OrderType type,
            BigDecimal notional,
            BigDecimal submittedRate,
            Instant submittedAt,
            OrderStatus status,
            Instant executedAt,
            BigDecimal executedRate
    ) {
        super(tradeId, currencyPair, submittedAt, user);
        _user = user;
        _side = side;
        _type = type;
        _notional = notional.setScale(2, BigDecimal.ROUND_FLOOR);
        _submittedRate = submittedRate;
        _executedAt = executedAt;
        _status = status;
        _executedRate = executedRate;
    }

    public User getUser() {
        return _user;
    }

    public Side getSide() {
        return _side;
    }

    public OrderType getType() {
        return _type;
    }

    public BigDecimal getNotional() {
        return _notional;
    }

    public BigDecimal getSubmittedRate() {
        return _submittedRate;
    }

    public Instant getExecutedAt() {
        return _executedAt;
    }

    public void setExecutedAt(Instant executedAt) {
        _executedAt = executedAt;
    }

    public OrderStatus getStatus() {
        return _status;
    }

    public void setStatus(OrderStatus status) {
        _status = status;
    }

    public BigDecimal getExecutedRate() {
        return _executedRate;
    }

    public void setExecutedRate(BigDecimal executedRate) {
        _executedRate = executedRate;
    }

    @Override
    public String toString() {
        return "SimpleOrder{" +
                "_tradeId='" + getTradeId() + '\'' +
                ", _currencyPair=" + getCurrencyPair().getSymbol() +
                ", _executedAt=" + getSubmittedAt() +
                ", _user=" + _user +
                ", _side=" + _side +
                ", _type=" + _type +
                ", _notional=" + _notional +
                ", _submittedRate=" + _submittedRate +
                ", _executedAt=" + _executedAt +
                ", _status=" + _status +
                ", _executedRate=" + _executedRate +
                '}';
    }
}