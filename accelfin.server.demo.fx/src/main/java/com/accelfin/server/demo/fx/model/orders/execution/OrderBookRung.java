package com.accelfin.server.demo.fx.model.orders.execution;

import java.math.BigDecimal;

public class OrderBookRung {
    private BigDecimal _marketSizeNotional;
    private BigDecimal _rate;
    private OrderBookRungType _type;

    public OrderBookRung(BigDecimal marketSizeNotional, BigDecimal rate, OrderBookRungType type) {
        _marketSizeNotional = marketSizeNotional.setScale(2, BigDecimal.ROUND_FLOOR);
        _rate = rate;
        _type = type;
    }

    public BigDecimal getMarketSizeNotional() {
        return _marketSizeNotional;
    }

    public BigDecimal getRate() {
        return _rate;
    }

    public OrderBookRungType getType() {
        return _type;
    }

    @Override
    public String toString() {
        return "OrderBookRung{" +
                "_marketSizeNotional=" + _marketSizeNotional +
                ", _type=" + _type +
                ", _rate=" + _rate +
                '}';
    }
}