package com.accelfin.server.demo.fx.dtos.mappers;

import com.accelfin.server.demo.fx.dtos.BlotterUpdateTypeDto;
import com.accelfin.server.demo.fx.model.common.BlotterUpdateType;

public class BlotterUpdateTypeMapper {
    public static BlotterUpdateTypeDto mapToDto(BlotterUpdateType blotterUpdateType) {
        switch (blotterUpdateType) {
            case Normal:
                return BlotterUpdateTypeDto.NORMAL;
            case StateOfTheWorld:
                return BlotterUpdateTypeDto.STATE_OF_THE_WORLD;
        }
        throw new RuntimeException("Unknown update type " + blotterUpdateType);
    }
}
