package com.accelfin.server.demo.fx.model.fakeData;

import com.accelfin.server.demo.fx.model.User;

import java.util.concurrent.ThreadLocalRandom;

public class FakeUsers {

    private final User _keith;
    private User[] _users;

    public static FakeUsers Instance = new FakeUsers();

    public FakeUsers() {
        _keith = new User("keith", "Keith", "Woods", new String[]{"fx-cash"});
        _users = new User[]{
                _keith,
                new User("ray", "Ray", "Booysen", new String[]{"fx-cash"}),
                new User("johan", "Johan", "Barnard", new String[]{"analytics"}),
                new User("zoe.carlisle", "Zoe", "Carlisle", new String[]{"fx-cash"}),
                new User("lee.geist", "Lee", "Geist", new String[]{"fx-cash"}),
                new User("tama.stumbo", "Tama", "Stumbo", new String[]{"fx-cash"}),
                new User("ethan.effler", "Ethan", "Effler", new String[]{"fx-cash"}),
                new User("melda.dreher", "Melda", "Dreher", new String[]{"fx-cash"}),
                new User("valerie.norsworthy", "Valerie", "Norsworthy", new String[]{"fx-cash"}),
                new User("archie.coronado", "Archie", "Coronado", new String[]{"fx-cash"}),
                new User("wilbur.northington", "Wilbur", "Northington", new String[]{"fx-cash"}),
                new User("cyrus.toone", "Cyrus", "Toone", new String[]{"fx-cash"}),
                new User("gil.barnett", "Gil", "Barnett", new String[]{"fx-cash"}),
                new User("chase.lesniak", "Chase", "Lesniak", new String[]{"fx-cash"}),
                new User("anh.luz", "Anh", "Luz", new String[]{"fx-cash"}),
                new User("carman.dauer", "Carman", "Dauer", new String[]{"fx-cash"}),
                new User("joella.knox", "Joella", "Knox", new String[]{"fx-cash"}),
                new User("julian.echevarria", "Julian", "Echevarria", new String[]{"fx-cash"}),
                new User("donte.muraoka", "Donte", "Muraoka", new String[]{"fx-cash"}),
                new User("akilah.nicola", "Akilah", "Nicola", new String[]{"fx-cash"}),
                new User("kyra.stangl", "Kyra", "Stangl", new String[]{"fx-cash"}),
                new User("tuyet.miah", "Tuyet", "Miah", new String[]{"fx-cash"}),
                new User("ilene.banach", "Ilene", "Banach", new String[]{"fx-cash"}),
                new User("dannette.mara", "Dannette", "Mara", new String[]{"fx-cash"}),
                new User("freida.wotring", "Freida", "Wotring", new String[]{"fx-cash"}),
                new User("verena.oscar", "Verena", "Oscar", new String[]{"fx-cash"}),
                new User("loriann.fettig", "Loriann", "Fettig", new String[]{"fx-cash"}),
                new User("tess.tibbets", "Tess", "Tibbets", new String[]{"fx-cash"}),
                new User("jordan.hem", "Jordan", "Hem", new String[]{"fx-cash"}),
                new User("silvia.junk", "Silvia", "Junk", new String[]{"fx-cash"}),
                new User("todd.wishon", "Todd", "Wishon", new String[]{"fx-cash"}),
                new User("woodrow.maiorano", "Woodrow", "Maiorano", new String[]{"fx-cash"}),
                new User("pamula.lone", "Pamula", "Lone", new String[]{"fx-cash"}),
                new User("thomasena.boss", "Thomasena", "Boss", new String[]{"fx-cash"}),
                new User("myrl.ruhl", "Myrl", "Ruhl", new String[]{"fx-cash"}),
                new User("geneva.patout", "Geneva", "Patout", new String[]{"fx-cash"}),
                new User("shelba.mceachern", "Shelba", "Mceachern", new String[]{"fx-cash"}),
                new User("deidre.gean", "Deidre", "Gean", new String[]{"fx-cash"}),
                new User("laurence.baggett", "Laurence", "Baggett", new String[]{"fx-cash"}),
                new User("cammy.wheeless", "Cammy", "Wheeless", new String[]{"fx-cash"}),
                new User("nelson.silvernail", "Nelson", "Silvernail", new String[]{"fx-cash"}),
                new User("gaynelle.yeldell", "Gaynelle", "Yeldell", new String[]{"fx-cash"}),
                new User("alphonso.pfeffer", "Alphonso", "Pfeffer", new String[]{"fx-cash"}),
                new User("tula.meredith", "Tula", "Meredith", new String[]{"fx-cash"}),
                new User("tomoko.fairbank", "Tomoko", "Fairbank", new String[]{"fx-cash"}),
                new User("deidra.willcox", "Deidra", "Willcox", new String[]{"fx-cash"}),
                new User("gracia.dohrmann", "Gracia", "Dohrmann", new String[]{"fx-cash"}),
                new User("myrle.dollison", "Myrle", "Dollison", new String[]{"fx-cash"}),
                new User("an.rigdon", "An", "Rigdon", new String[]{"fx-cash"}),
                new User("eulah.kalina", "Eulah", "Kalina", new String[]{"fx-cash"}),
                new User("thelma.rothermel", "Thelma", "Rothermel", new String[]{"fx-cash"}),
                new User("hazel.endsley", "Hazel", "Endsley", new String[]{"fx-cash"}),
                new User("cherri.ruble", "Cherri", "Ruble", new String[]{"fx-cash"})
        };
    }

    public User getKeith() {
        return _keith;
    }

    public User[] getUsers() {
        return _users;
    }

    public User getRandomUser() {
        ThreadLocalRandom random = ThreadLocalRandom.current();
        return _users[random.nextInt(_users.length - 1)];
    }
}
