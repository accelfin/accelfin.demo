package com.accelfin.server.demo.fx.model.cash.pricing;

import com.esp.disposables.DisposableBase;
import com.accelfin.server.demo.fx.model.events.FxSpotPriceEvent;

import java.time.Instant;
import java.util.*;
import java.util.stream.Collectors;

public class PriceRepository extends DisposableBase {
    NavigableMap<Instant, ArrayList<FxSpotPriceEvent>> _prices = new TreeMap<>();
    Map<String, FxSpotPriceEvent> _pricesById = new HashMap<>();
    Map<String, FxSpotPriceEvent> _pricesBySymbol = new HashMap<>();

    public FxSpotPriceEvent getPrice(String id) {
        return _pricesById.getOrDefault(id, FxSpotPriceEvent.None);
    }

    public boolean hasLastPrice(String symbol) {
        return _pricesBySymbol.containsKey(symbol);
    }

    public FxSpotPriceEvent getLastPrice(String symbol) {
        return _pricesBySymbol.get(symbol);
    }

    public List<FxSpotPriceEvent> getPrices(HashSet<String> symbols, Instant start, Instant end)
    {
        SortedMap<Instant, ArrayList<FxSpotPriceEvent>> range = _prices.subMap(
                start,
                true,
                end,
                true
        );
        return range
                .values()
                .stream()
                .flatMap(List::stream)
                .filter(p-> symbols.contains(p.getSymbol()))
                .collect(Collectors.toList());
    }

    public int cacheSize() {
        return _prices.size();
    }

    public void put(FxSpotPriceEvent price) {
        // TODO this is a massively inefficient method of storing prices
        ArrayList<FxSpotPriceEvent> prices = _prices.get(price.getCreationDate());
        if(prices == null) {
            prices = new ArrayList<>();
            _prices.put(price.getCreationDate(), prices);
        }
        prices.add(price);
        _pricesById.put(price.getId(), price);
        _pricesBySymbol.put(price.getSymbol(), price);
    }

    public Collection<FxSpotPriceEvent> getStartPrices(HashSet<String> symbols, Instant start) {
        HashSet<String> symbolsCopy = new HashSet<>(symbols);
        ArrayList<FxSpotPriceEvent> results = new ArrayList<>();
        SortedMap<Instant, ArrayList<FxSpotPriceEvent>> range = _prices.tailMap(
                start,
                true
        );
        // TODO this is a massively inefficient method of storing prices
        for(ArrayList<FxSpotPriceEvent> prices : range.values()) {
            for(FxSpotPriceEvent price : prices) {
                if (symbolsCopy.contains(price.getSymbol())) {
                    results.add(price);
                    symbolsCopy.remove(price.getSymbol());
                }
                if (symbolsCopy.size() == 0) {
                    break;
                }
            }
        }
        return results;
    }
}
