package com.accelfin.server.demo.fx.dtos.mappers;

import com.accelfin.server.messaging.mappers.InboundMessageEventMapperBase;
import com.accelfin.server.messaging.MessageEnvelope;
import com.accelfin.server.messaging.events.InboundMessageEvent;
import com.accelfin.server.demo.FxOperationNameConst;
import com.google.inject.Inject;

public class FxServiceInboundMessageEventMapper extends InboundMessageEventMapperBase {

    private SessionMapper _sessionMapper;
    private PriceMapper _priceMapper;
    private CashExecutionMapper _cashExecutionMapper;
    private CashBlotterMapper _cashBlotterMapper;
    private HistoricalPriceMapper _historicalPriceMapper;
    private HistoricalTradeMapper _historicalTradeMapper;
    private OrderBlotterMapper _orderBlotterMapper;
    private OrderBookMapper _orderBookMapper;
    private OrderExecutionMapper _orderExecutionMapper;

    @Inject
    public FxServiceInboundMessageEventMapper(
            SessionMapper sessionMapper,
            PriceMapper priceMapper,
            CashExecutionMapper cashExecutionMapper,
            CashBlotterMapper cashBlotterMapper,
            HistoricalPriceMapper historicalPriceMapper,
            HistoricalTradeMapper historicalTradeMapper,
            OrderBlotterMapper orderBlotterMapper,
            OrderBookMapper orderBookMapper,
            OrderExecutionMapper orderExecutionMapper
    ) {
        _sessionMapper = sessionMapper;
        _priceMapper = priceMapper;
        _cashExecutionMapper = cashExecutionMapper;
        _cashBlotterMapper = cashBlotterMapper;
        _historicalPriceMapper = historicalPriceMapper;
        _historicalTradeMapper = historicalTradeMapper;
        _orderBlotterMapper = orderBlotterMapper;
        _orderBookMapper = orderBookMapper;
        _orderExecutionMapper = orderExecutionMapper;
    }

    @Override
    protected InboundMessageEvent tryMapToEvent(MessageEnvelope envelope) {
        InboundMessageEvent result;
        switch (envelope.getOperationName()) {
            case FxOperationNameConst.Login:
                result = _sessionMapper.mapLoginOperationEvent(envelope);
                break;
            case FxOperationNameConst.PriceSubscriptionUpdate:
                result = _priceMapper.mapPriceSubscriptionUpdateOperationEvent(envelope);
                break;
            case FxOperationNameConst.ExecuteSpot:
                result = _cashExecutionMapper.mapExecuteSpotOperationEvent(envelope);
                break;
            case FxOperationNameConst.CashBlotterSubscribe:
                result = _cashBlotterMapper.mapBlotterSubscribeOperationEvent(envelope);
                break;
            case FxOperationNameConst.ClientHeartbeat:
                result = _sessionMapper.mapClientHeartbeatOperationEvent(envelope);
                break;
            case FxOperationNameConst.GetStartingPrices:
                result = _historicalPriceMapper.mapGetStartPricesEvent(envelope);
                break;
            case FxOperationNameConst.GetHistoricalPrices:
                result = _historicalPriceMapper.mapGetHistoricalPricesEvent(envelope);
                break;
            case FxOperationNameConst.GetHistoricalTrades:
                result = _historicalTradeMapper.mapGetHistoricalTradesEvent(envelope);
                break;
            case FxOperationNameConst.ExecuteSimpleOrder:
                result = _orderExecutionMapper.mapExecuteOrderOperationEvent(envelope);
                break;
            case FxOperationNameConst.OrderBlotterSubscribe:
                result = _orderBlotterMapper.mapOrderBlotterSubscribeOperationEvent(envelope);
                break;
            case FxOperationNameConst.OrderBookSubscribe:
                result = _orderBookMapper.mapOrderBookSubscriptionUpdateRequestDtoToOperationEvent(envelope);
                break;
            default:
                throw new RuntimeException("Unknown operation name " + envelope.getOperationName());
        }
        return result;
    }
}