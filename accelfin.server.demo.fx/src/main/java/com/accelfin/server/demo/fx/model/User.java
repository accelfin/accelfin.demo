package com.accelfin.server.demo.fx.model;

import java.math.BigDecimal;
import java.util.HashMap;

public class User {
    private String _userName;
    private String _firstName;
    private String _lastName;
    private String[] _permissions;
    private HashMap<String, BigDecimal> _liquidityByCurrencyPair = new HashMap<>();

    public User(String userName, String firstName, String lastName, String[] permissions) {
        _userName = userName;
        _firstName = firstName;
        _lastName = lastName;
        _permissions = permissions;
    }

    public String getUserName() {
        return _userName;
    }

    public String getFirstName() {
        return _firstName;
    }

    public String getLastName() {
        return _lastName;
    }

    public String[] getPermissions() {
        return _permissions;
    }

    public boolean hasLiquidity(String currencyPair) {
        return getLiquidity(currencyPair).compareTo(BigDecimal.ZERO) > 0;
    }

    public BigDecimal getLiquidity(String currencyPair) {
        BigDecimal liquidity = _liquidityByCurrencyPair.get(currencyPair);
        if(liquidity == null) {
            liquidity = BigDecimal.valueOf(1000000);
            _liquidityByCurrencyPair.put(currencyPair, liquidity);
        }
        return liquidity;
    }

    public void setLiquidity(String currencyPair, BigDecimal liquidity) {
        _liquidityByCurrencyPair.put(currencyPair, liquidity);
    }

    public boolean decreaseLiquidity(String currencyPair, BigDecimal amount) {
        BigDecimal liquidity = getLiquidity(currencyPair);
        if (liquidity.subtract(amount).compareTo(BigDecimal.ZERO) < 0) {
            return false;
        }
        liquidity = liquidity.subtract(amount);
        _liquidityByCurrencyPair.put(currencyPair, liquidity);
        return true;
    }
}
