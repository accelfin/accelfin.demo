package com.accelfin.server.demo.fx.model.fakeData;

import com.accelfin.server.Clock;
import com.esp.disposables.DisposableBase;
import com.accelfin.server.messaging.events.ClockTickEvent;
import com.accelfin.server.demo.common.model.CurrencyPair;
import com.accelfin.server.demo.fx.model.FxService;
import com.accelfin.server.demo.fx.model.events.FxSpotPriceEvent;
import com.esp.ObserveEvent;
import com.esp.Router;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.time.Instant;
import java.time.ZoneOffset;
import java.util.Iterator;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

public class CurrencyPairPriceGenerator extends DisposableBase {
    private static Logger logger = LoggerFactory.getLogger(CurrencyPairPriceGenerator.class);

    private final Router<FxService> _router;
    private final CurrencyPair _currencyPair;
    private final Clock _clock;
    private final int _frequency;
    private int _priceGeneratedCount;
    private Instant _lastGeneratedTime;
    private Iterator<BidAndAsk> _priceListIterator;

    CurrencyPairPriceGenerator(
            Router<FxService> router,
            CurrencyPair pair,
            Clock clock
    ) {
        _router = router;
        _currencyPair = pair;
        _clock = clock;
        _frequency = CurrencyPairsMetadata.Instance.getTickFrequencyMs(pair.getSymbol());
        _priceListIterator = PricesListManager.getPriceList(_currencyPair.getSymbol()).iterator();
    }

    public void observeEvents() {
        addDisposable(_router.observeEventsOn(this));
    }

    @ObserveEvent(eventClass = ClockTickEvent.class)
    public void onClockTickEvent() {
        FxSpotPriceEvent nextPrice = getNextPrice(_clock.now());
        if (nextPrice != null) {
            _router.publishEvent(nextPrice);
        }
    }

    public FxSpotPriceEvent getNextPrice(Instant now) {
        boolean shouldGenerate = _lastGeneratedTime == null || now.toEpochMilli() > TimeUnit.MILLISECONDS.convert(_lastGeneratedTime.getEpochSecond(), TimeUnit.SECONDS) + _frequency;
        if (!shouldGenerate) {
            return null;
        }

        BidAndAsk next = _priceListIterator.next();

        BigDecimal newAsk = next.getAsk().setScale(_currencyPair.getRatePrecision(), BigDecimal.ROUND_UP);
        BigDecimal newBid = next.getBid().setScale(_currencyPair.getRatePrecision(), BigDecimal.ROUND_DOWN);
        BigDecimal newSpread = newAsk.subtract(newBid);
        BigDecimal newMid = newBid.add(newSpread.divide(BigDecimal.valueOf(2), BigDecimal.ROUND_HALF_EVEN));
        String id = UUID.randomUUID().toString();
        BigDecimal displaySpread = newSpread.multiply(BigDecimal.valueOf(10).pow(_currencyPair.getPipsPosition())).setScale(1, BigDecimal.ROUND_HALF_EVEN);

        FxSpotPriceEvent nextPrice = new FxSpotPriceEvent(
                String.format("%s-%s", _currencyPair.getSymbol(), id),
                _currencyPair.getSymbol(),
                getSpotDate(),
                newMid,
                newAsk,
                newBid,
                displaySpread,
                now,
                _currencyPair.isEnabled()
        );
        _priceGeneratedCount++;
        _lastGeneratedTime = now;
        return nextPrice;
    }

    private Instant getSpotDate() {
        return _clock.localDateNow().atStartOfDay().toInstant(ZoneOffset.UTC);
    }

    String getSummary() {
        return String.format("%s - %s fake prices", _currencyPair.getSymbol(), _priceGeneratedCount);
    }
}