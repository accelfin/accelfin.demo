package com.accelfin.server.demo.fx.model.fakeData;

import com.accelfin.server.Clock;
import com.accelfin.server.messaging.events.InitEvent;
import com.accelfin.server.demo.common.model.CurrencyPair;
import com.accelfin.server.demo.fx.model.FxService;
import com.accelfin.server.demo.fx.model.cash.blotter.SpotTrade;
import com.accelfin.server.demo.fx.model.cash.pricing.PriceRepository;
import com.accelfin.server.demo.fx.model.common.TradeRepository;
import com.accelfin.server.demo.fx.model.events.FxSpotPriceEvent;
import com.esp.ObserveEvent;
import com.esp.Router;
import com.esp.disposables.DisposableBase;
import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.HashMap;

public class FakeActivities extends DisposableBase {
    private static Logger logger = LoggerFactory.getLogger(FakeActivities.class);

    private Router<FxService> _router;
    private Clock _clock;
    private final PriceRepository _priceRepository;
    private final TradeRepository<SpotTrade> _tradeRepository;
    private SpotTradeGenerator _spotTradeGenerator;
    private ArrayList<CurrencyPairPriceGenerator> _currencyPairPriceGenerators = new ArrayList<>();
    private HashMap<String, CurrencyPairPriceGenerator> _currencyPairPriceGeneratorsBySymbol = new HashMap<>();

    @Inject
    public FakeActivities(
            Router<FxService> router,
            Clock clock,
            PriceRepository priceRepository,
            TradeRepository<SpotTrade> tradeRepository
    ) {
        _router = router;
        _clock = clock;
        _priceRepository = priceRepository;
        _tradeRepository = tradeRepository;
        _spotTradeGenerator = new SpotTradeGenerator(router, clock, priceRepository);
        createFakePriceGenerators();
    }

    public void observeEvents() {
        addDisposable(_router.observeEventsOn(this));
        _currencyPairPriceGenerators.forEach(CurrencyPairPriceGenerator::observeEvents);
        _spotTradeGenerator.observeEvents();
    }

    public CurrencyPairPriceGenerator getPriceGenerator(String symbol) {
        return _currencyPairPriceGeneratorsBySymbol.get(symbol);
    }

    @ObserveEvent(eventClass = InitEvent.class)
    public void onInitEvent() {
    }

    private void createFakePriceGenerators() {
        logger.debug("Fake batch provider started");
        for(CurrencyPair currencyPair : CurrencyPairsMetadata.Instance.getPairs()) {
            CurrencyPairPriceGenerator currencyPairPriceGenerator = new CurrencyPairPriceGenerator(
                    _router,
                    currencyPair,
                    _clock
            );
            _currencyPairPriceGenerators.add(currencyPairPriceGenerator);
            _currencyPairPriceGeneratorsBySymbol.put(currencyPair.getSymbol(), currencyPairPriceGenerator);
            addDisposable(currencyPairPriceGenerator);
        }
    }

    public void preLoadModel() {
        Instant now = _clock.now().minus(8, ChronoUnit.HOURS);
        boolean isHistorical = true;
        while (isHistorical) {
            now = now.plus(100, ChronoUnit.MILLIS);
            for(CurrencyPairPriceGenerator generator : _currencyPairPriceGenerators) {
                FxSpotPriceEvent price = generator.getNextPrice(now);
                if(price != null) {
                    _priceRepository.put(price);
                }
            }
            SpotTrade trade = _spotTradeGenerator.getNextTrade(now);
            if(trade != null) {
                _tradeRepository.add(trade);
            }
            isHistorical = now.toEpochMilli() < _clock.now().toEpochMilli();
        }
        StringBuilder logMessage = new StringBuilder("Finished generating fake data").append("\n");
        for(CurrencyPairPriceGenerator generator : _currencyPairPriceGenerators) {
            logMessage.append(generator.getSummary()).append("\n");
        }
        logMessage.append(_spotTradeGenerator.getSummary()).append("\n");
        logger.info(logMessage.toString());
    }
}