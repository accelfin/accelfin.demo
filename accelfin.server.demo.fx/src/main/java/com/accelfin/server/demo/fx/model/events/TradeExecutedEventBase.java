package com.accelfin.server.demo.fx.model.events;

import com.accelfin.server.demo.fx.model.common.TradeBase;

public class TradeExecutedEventBase<TTrade extends TradeBase> {
    private TTrade _trade;

    public TradeExecutedEventBase(TTrade trade) {
        _trade = trade;
    }

    public TTrade getTrade() {
        return _trade;
    }
}
