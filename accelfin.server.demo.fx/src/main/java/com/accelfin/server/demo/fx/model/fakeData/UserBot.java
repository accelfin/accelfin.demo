package com.accelfin.server.demo.fx.model.fakeData;

import com.accelfin.server.demo.common.model.CurrencyPair;
import com.accelfin.server.demo.common.model.Side;
import com.accelfin.server.demo.fx.model.User;
import com.accelfin.server.demo.fx.model.cash.blotter.SpotTrade;
import com.accelfin.server.demo.fx.model.events.FxSpotPriceEvent;
import com.accelfin.server.demo.fx.model.events.SimpleOrderExecutedEvent;
import com.accelfin.server.demo.fx.model.events.SpotTradeExecutedEvent;
import com.accelfin.server.demo.fx.model.orders.blotter.OrderStatus;
import com.accelfin.server.demo.fx.model.orders.blotter.OrderType;
import com.accelfin.server.demo.fx.model.orders.blotter.SimpleOrder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.time.Duration;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.UUID;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;

///**
// * Created by keith on 31/07/16.
// */
public class UserBot {
    private static Logger logger = LoggerFactory.getLogger(UserBot.class);

//    private void genSpotTrades(DefaultFakeDataBatch tempData) {
//        ThreadLocalRandom random = ThreadLocalRandom.current();
//        double hours = ChronoUnit.MINUTES.between(_batchStartTime, _batchEndTime) / 60d;
//        long numberOfTrades = (long) (hours * _settings.getNumberOfFakeTradesPerHour());
//        if(numberOfTrades == 0 ) {
//            return;
//        }
//        long numberOfAlertTrades = (long) (hours * _settings.getNumberOfAlertTradesPerHour());
//        ArrayList<SpotExecutions> bookAtSeconds = random
//                .longs(numberOfTrades, _batchStartTime.getEpochSecond(), _batchEndTime.getEpochSecond())
//                .sorted()
//                .mapToObj(seconds -> new SpotExecutions(Instant.ofEpochSecond(seconds), null))
//                .collect(Collectors.toCollection(ArrayList::new));
//        if(numberOfAlertTrades > 0) {
//            int[] tradeAlertIndexes = random.ints(numberOfAlertTrades, 0, bookAtSeconds.size()).sorted().toArray();
//            for (int tradeAlertIndex : tradeAlertIndexes) {
//                bookAtSeconds.get(tradeAlertIndex).isAlertTrade = true;
//            }
//        }
//        ArrayDeque<SpotExecutions> bookAtSecondsStack = bookAtSeconds.getStreamOperation().collect(Collectors.toCollection(ArrayDeque::new));
//        SpotExecutions nextBooking = bookAtSecondsStack.pop();
//        logger.debug("{}: Will book {} fake trades, of which {} should flag alerts", getBatchDurationLogMessage(), numberOfTrades, numberOfAlertTrades);
//        for (FxSpotPriceEvent price : tempData.PriceEvents) {
//            if (nextBooking != null && nextBooking.bookAtTime.isAfter(price.getCreationDate())) {
//                tempData.addTrade(createSpotTradeExecutedEvent(price, nextBooking.bookAtTime, nextBooking.isAlertTrade));
//                if (bookAtSecondsStack.peek() != null) {
//                    nextBooking = bookAtSecondsStack.pop();
//                } else {
//                    nextBooking = null;
//                }
//            }
//        }
//    }
//
//    private void genOrders(DefaultFakeDataBatch tempData) {
//        ThreadLocalRandom random = ThreadLocalRandom.current();
//        double hours = ChronoUnit.MINUTES.between(_batchStartTime, _batchEndTime) / 60d;
//        long numberOfOrders = (long) (hours * _settings.getNumberOfFakeOrdersPerHour());
//        if(numberOfOrders == 0 ) {
//            return;
//        }
//        ArrayList<SimpleOrderExecutions> bookAtSeconds = random
//                .longs(numberOfOrders, _batchStartTime.getEpochSecond(), _batchEndTime.getEpochSecond())
//                .sorted()
//                .mapToObj(seconds -> new SimpleOrderExecutions(Instant.ofEpochSecond(seconds), null))
//                .collect(Collectors.toCollection(ArrayList::new));
//        ArrayDeque<SimpleOrderExecutions> bookAtSecondsStack = bookAtSeconds.getStreamOperation().collect(Collectors.toCollection(ArrayDeque::new));
//        SimpleOrderExecutions nextBooking = bookAtSecondsStack.pop();
//        logger.debug("{}: Will book {} fake orders", getBatchDurationLogMessage(), numberOfOrders);
//        for (FxSpotPriceEvent price : tempData.PriceEvents) {
//            if (nextBooking != null && nextBooking.bookAtTime.isAfter(price.getCreationDate())) {
//                // At this point we know that our order will be filled at or before we hit this 'price'.
//                // Given this we push the booking time back so we have some trades in our backlog.
//                // wWe can only push it back to the start of the batch.
//                // It's important to respect teh batch bounds else we'd be booking items for the last batch and would get some odd behaviour as the system moves time forward
//                // (i.e. loads of trades appearing but booked in the past when we roll to the next batch.
//                Duration startToBookAtTimeDelta = Duration.between(_batchStartTime, nextBooking.bookAtTime);
//                Instant bookAtTime = nextBooking.bookAtTime.minus(random.nextLong(1, startToBookAtTimeDelta.getSeconds()), ChronoUnit.SECONDS);
//                tempData.addSimpleOrder(createSimpleOrderEvent(price, bookAtTime));
//                if (bookAtSecondsStack.peek() != null) {
//                    nextBooking = bookAtSecondsStack.pop();
//                } else {
//                    nextBooking = null;
//                }
//            }
//        }
//    }
//
//    private SpotTradeExecutedEvent createSpotTradeExecutedEvent(FxSpotPriceEvent price, Instant bookedAtTime, boolean isAlertTrade) {
//        CurrencyPair currencyPair = _referenceData.getCurrencyPair(price.getSymbol());
//        Side side = bookedAtTime.getEpochSecond() % 2 == 0
//                ? Side.Buy
//                : Side.Sell;
//        BigDecimal rate = side == Side.Buy
//                ? price.getAsk()
//                : price.getBid();
//        if (isAlertTrade) {
//            rate = price.getMid();
//        }
//        BigDecimal notional = BigDecimal.valueOf(ThreadLocalRandom.current().nextInt(10000, 2000000) / 1000 * 1000);
//        String tradeId = UUID.randomUUID().toString();
//        SpotTrade trade = new SpotTrade(
//                tradeId,
//                currencyPair,
//                notional,
//                side,
//                price.getId(),
//                rate,
//                bookedAtTime,
//                FakeUsers.Instance.getRandomUser()
//        );
//        // logger.debug("Fake trade:[{}]", trade);
//
//        // we assume if the current batch start/end settings are in the past then
//        // this trade can be treated as historical
//        boolean isStateOfTheWorld = trade.getSubmittedAt().isBefore(_clock.now());
//        return new SpotTradeExecutedEvent(trade, isStateOfTheWorld);
//    }
//
//    private SimpleOrderExecutedEvent createSimpleOrderEvent(FxSpotPriceEvent price, Instant bookAtTime) {
//        ThreadLocalRandom random = ThreadLocalRandom.current();
//        int userIndex = random.nextInt(0, _referenceData.getUsers().size() - 1);
//        Side side = random.nextInt(0, 1) == 0 ? Side.Buy : Side.Sell;
//        OrderType orderType = OrderType.class.getEnumConstants()[random.nextInt(0, 2)];
//        User user = _referenceData.getUsers().get(userIndex);
//        CurrencyPair currencyPair = _referenceData.getCurrencyPair(price.getSymbol());
//        BigDecimal notional = BigDecimal.valueOf(random.nextInt(1000, 100000));
//        BigDecimal rate = side == Side.Buy ? price.getAsk() : price.getBid();
//        SimpleOrder simpleOrder = new SimpleOrder(
//                UUID.randomUUID().toString(),
//                currencyPair,
//                user,
//                side,
//                orderType,
//                notional,
//                rate,
//                bookAtTime,
//                OrderStatus.ACTIVE,
//                null,
//                null
//        );
//        logger.info("Order will book at {}", bookAtTime);
//        boolean isStateOfTheWorld = bookAtTime.isBefore(_clock.now());
//        return new SimpleOrderExecutedEvent(simpleOrder, isStateOfTheWorld);
//    }
}
