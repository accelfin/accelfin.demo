package com.accelfin.server.demo.fx.container;

import com.accelfin.server.Clock;
import com.accelfin.server.Scheduler;
import com.accelfin.server.ServiceModelBase;
import com.accelfin.server.demo.FxOperationNameConst;
import com.accelfin.server.demo.common.messaging.ConnectionIds;
import com.accelfin.server.demo.fx.dtos.mappers.FxServiceInboundMessageEventMapper;
import com.accelfin.server.messaging.Connections;
import com.accelfin.server.messaging.ConnectionsBuilder;
import com.accelfin.server.messaging.config.SerializationType;
import com.accelfin.server.messaging.config.operations.OperationOwnership;
import com.accelfin.server.messaging.config.operations.OperationType;
import com.accelfin.server.messaging.gateways.MqGateway;
import com.esp.Router;
import com.google.inject.Inject;
import com.google.inject.Provider;

public class ConnectionsProvider implements Provider<Connections> {
    private Router<ServiceModelBase> _router;
    private Clock _clock;
    private Scheduler _scheduler;
    private FxServiceInboundMessageEventMapper _eventMapper;

    @Inject
    public ConnectionsProvider(
            Router<ServiceModelBase> router,
            Clock clock,
            Scheduler scheduler,
            FxServiceInboundMessageEventMapper eventMapper
    ) {
        _router = router;
        _clock = clock;
        _scheduler = scheduler;
        _eventMapper = eventMapper;
    }

    protected MqGateway mqGatewayOverride() {
        // this is here for testing.
        // if null the messaging layer will default it
        return null;
    }

    @Override
    public Connections get() {
        return ConnectionsBuilder.create()
                .withConfiguration("messaging.properties")
                .withClock(_clock)
                .withRouter(_router)
                .withConnection(ConnectionIds.WEB,cb -> cb
                        .withSerializationType(SerializationType.Json)
                        .withInboundMessageEventMapper(_eventMapper)
                        .withMqGateway(mqGatewayOverride())
                        .withOperation(
                                OperationOwnership.OwnedByThisService,
                                OperationType.RPC,
                                FxOperationNameConst.Login)
                        .withOperation(
                                OperationOwnership.OwnedByThisService,
                                OperationType.RPC,
                                FxOperationNameConst.PriceSubscriptionUpdate,
                                true)
                        .withOperation(
                                OperationOwnership.OwnedByThisService,
                                OperationType.Stream,
                                FxOperationNameConst.PriceUpdate,
                                true)
                        .withOperation(
                                OperationOwnership.OwnedByThisService,
                                OperationType.RPC,
                                FxOperationNameConst.CashBlotterSubscribe,
                                true)
                        .withOperation(
                                OperationOwnership.OwnedByThisService,
                                OperationType.Stream,
                                FxOperationNameConst.CashBlotterUpdate,
                                true)
                        .withOperation(
                                OperationOwnership.OwnedByThisService,
                                OperationType.RPC,
                                FxOperationNameConst.ExecuteSpot,
                                true)
                        .withOperation(
                                OperationOwnership.OwnedByThisService,
                                OperationType.RPC,
                                FxOperationNameConst.ClientHeartbeat,
                                true)
                        .withOperation(
                                OperationOwnership.OwnedByThisService,
                                OperationType.RPC,
                                FxOperationNameConst.ExecuteSimpleOrder,
                                true)
                        .withOperation(
                                OperationOwnership.OwnedByThisService,
                                OperationType.RPC,
                                FxOperationNameConst.OrderBlotterSubscribe,
                                true)
                        .withOperation(
                                OperationOwnership.OwnedByThisService,
                                OperationType.Stream,
                                FxOperationNameConst.OrderBlotterUpdate,
                                true)
                        .withOperation(
                                OperationOwnership.OwnedByThisService,
                                OperationType.RPC,
                                FxOperationNameConst.OrderBookSubscribe,
                                true)
                        .withOperation(
                                OperationOwnership.OwnedByThisService,
                                OperationType.Stream,
                                FxOperationNameConst.OrderBookUpdate,
                                true))
                .withConnection("internal", cb -> cb
                        .withSerializationType(SerializationType.Binary)
                        .withInboundMessageEventMapper(_eventMapper)
                        .withMqGateway(mqGatewayOverride())
                        .withOperation(
                                OperationOwnership.OwnedByThisService,
                                OperationType.RPC,
                                FxOperationNameConst.GetStartingPrices
                        )
                        .withOperation(
                                OperationOwnership.OwnedByThisService,
                                OperationType.RPC,
                                FxOperationNameConst.GetHistoricalPrices
                        )
                        .withOperation(
                                OperationOwnership.OwnedByThisService,
                                OperationType.RPC,
                                FxOperationNameConst.GetHistoricalTrades
                        ))
                .build();
    }
}