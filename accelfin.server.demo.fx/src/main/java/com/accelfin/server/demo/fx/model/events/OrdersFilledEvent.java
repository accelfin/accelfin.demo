package com.accelfin.server.demo.fx.model.events;

import com.accelfin.server.demo.fx.model.orders.blotter.SimpleOrder;

import java.util.List;

public class OrdersFilledEvent {
    private List<SimpleOrder> _orders;

    public OrdersFilledEvent(List<SimpleOrder> orders) {
        _orders = orders;
    }

    public List<SimpleOrder> getOrders() {
        return _orders;
    }
}
