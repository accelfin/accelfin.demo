package com.accelfin.server.demo.fx.model.cash.pricing;

import com.accelfin.server.demo.common.messaging.ConnectionBroker;
import com.accelfin.server.SessionToken;
import com.accelfin.server.messaging.events.InitEvent;
import com.accelfin.server.demo.FxOperationNameConst;
import com.accelfin.server.demo.fx.dtos.GetHistoricalPricesResponseDto;
import com.accelfin.server.demo.fx.dtos.GetStartingPricesResponseDto;
import com.accelfin.server.demo.fx.dtos.mappers.HistoricalPriceMapper;
import com.accelfin.server.demo.fx.dtos.mappers.PriceMapper;
import com.accelfin.server.demo.fx.model.FxService;
import com.accelfin.server.demo.fx.model.events.FxSpotPriceEvent;
import com.accelfin.server.demo.fx.model.events.SessionsExpiredEvent;
import com.accelfin.server.demo.fx.model.events.operationEvents.GetHistoricalPricesEvent;
import com.accelfin.server.demo.fx.model.events.operationEvents.GetStartingPricesEvent;
import com.accelfin.server.demo.fx.model.events.operationEvents.PriceSubscriptionUpdateOperationEvent;
import com.accelfin.server.demo.fx.model.staticData.ReferenceData;
import com.esp.EventContext;
import com.esp.ObserveEvent;
import com.esp.Router;
import com.esp.disposables.DisposableBase;
import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.stream.Collectors;

public class Pricer extends DisposableBase {

    private static Logger logger = LoggerFactory.getLogger(Pricer.class);

    private Router<FxService> _router;
    private PriceMapper _priceMapper;
    private HistoricalPriceMapper _historicalPriceMapper;
    private ConnectionBroker _connectionBroker;
    private PriceRepository _priceRepository;
    private ReferenceData _referenceData;
    private HashMap<String, HashSet<SessionToken>> _subscribersByCcySymbol = new HashMap<>();

    @Inject
    public Pricer(
            Router<FxService> router,
            PriceMapper priceMapper,
            HistoricalPriceMapper historicalPriceMapper,
            ConnectionBroker connectionBroker,
            PriceRepository priceRepository,
            ReferenceData referenceData
    ) {
        _router = router;
        _priceMapper = priceMapper;
        _historicalPriceMapper = historicalPriceMapper;
        _connectionBroker = connectionBroker;
        _priceRepository = priceRepository;
        _referenceData = referenceData;
    }

    public void observeEvents() {
        addDisposable(_router.observeEventsOn(this));
    }

    @ObserveEvent(eventClass = InitEvent.class)
    public void onInitEvent() {
        _connectionBroker.setOperationStatus(FxOperationNameConst.GetHistoricalPrices, true);
        _connectionBroker.setOperationStatus(FxOperationNameConst.PriceSubscriptionUpdate, true);
        _connectionBroker.setOperationStatus(FxOperationNameConst.PriceUpdate, true);
        _connectionBroker.setOperationStatus(FxOperationNameConst.GetStartingPrices, true);
    }

    @ObserveEvent(eventClass = FxSpotPriceEvent.class)
    public void onFxSpotPriceReceivedEvent(FxSpotPriceEvent event, EventContext eventContext) {
        _priceRepository.put(event);
        String symbol = event.getSymbol();
        eventContext.commit();
        if (_subscribersByCcySymbol.containsKey(symbol)) {
            HashSet<SessionToken> subsscriptions = _subscribersByCcySymbol.get(symbol);
            SessionToken[] subscriberIds = subsscriptions.toArray(new SessionToken[subsscriptions.size()]);
            dispatchPrice(event, subscriberIds);
        } else {
            logger.trace("[{}] price received. Ignoring as there are no subscribers", symbol);
        }
    }

    @ObserveEvent(eventClass = PriceSubscriptionUpdateOperationEvent.class)
    public void onPriceSubscriptionOperationEvent(PriceSubscriptionUpdateOperationEvent event) {
        if (event.getIsOperationTermination()) {
            logger.debug("Subscription termination received");
            return;
        }
        _connectionBroker.sendRpcResponse(event,_priceMapper.mapSubscriptionSuccessResponse());
        SessionToken sessionToken = event.getSessionToken();
        logger.debug("Adding session [{}] to price streams [{}]", sessionToken, event.getSymbols());
        Set<String> usersSubscriptions = Arrays
                .stream(event.getSymbols())
                .collect(Collectors.toSet());
        for (String symbol : _subscribersByCcySymbol.keySet()) {
            HashSet<SessionToken> subscribers = _subscribersByCcySymbol.get(symbol);
            if (usersSubscriptions.contains(symbol)) {
                logger.debug("Session [{}] ADDED to price getStreamOperation [{}]", sessionToken, symbol);
                subscribers.add(sessionToken);
                usersSubscriptions.remove(symbol);
                if (_priceRepository.hasLastPrice(symbol)) {
                    FxSpotPriceEvent lastPrice = _priceRepository.getLastPrice(symbol);
                    dispatchPrice(lastPrice, sessionToken);
                }
            } else if (subscribers.contains(sessionToken)) {
                logger.debug("Session [{}] REMOVED from price getStreamOperation [{}]", sessionToken, symbol);
                subscribers.remove(sessionToken);
            }
        }
        for (String symbol : usersSubscriptions) {
            HashSet<SessionToken> subscriptions = _subscribersByCcySymbol.computeIfAbsent(symbol, s -> new HashSet<>());
            logger.debug("Session [{}] ADDED to price getStreamOperation [{}]", sessionToken, symbol);
            subscriptions.add(sessionToken);
            if (_priceRepository.hasLastPrice(symbol)) {
                FxSpotPriceEvent lastPrice = _priceRepository.getLastPrice(symbol);
                dispatchPrice(lastPrice, sessionToken);
            }
        }
    }

    @ObserveEvent(eventClass = SessionsExpiredEvent.class)
    public void onSessionsExpiredEvent(SessionsExpiredEvent event) {
        for (String symbol : _subscribersByCcySymbol.keySet()) {
            HashSet<SessionToken> subscriptions = _subscribersByCcySymbol.get(symbol);
            for (SessionToken sessionToken : event.getSessionTokens()) {
                if (subscriptions.contains(sessionToken)) {
                    logger.debug("Session expired removing from [{}]. session token [{}] ", symbol, sessionToken);
                    subscriptions.remove(sessionToken);
                }
            }
        }
    }

    private void dispatchPrice(FxSpotPriceEvent price, SessionToken... subscriberTokens) {
        logger.trace("[{}] price received. [{}]. Sending to [{}] subscribers", price.getSymbol(), price, subscriberTokens.length);
        _connectionBroker.sendStreamResponseMessage(
                FxOperationNameConst.PriceUpdate,
                _priceMapper.mapPriceUpdateResponseDto(price),
                false,
                subscriberTokens
        );
    }

    @ObserveEvent(eventClass = GetHistoricalPricesEvent.class)
    public void onGetPriceTicksRequestEvent(GetHistoricalPricesEvent event) {
        Collection<FxSpotPriceEvent> prices = _priceRepository.getPrices(
                new HashSet<>(Arrays.stream(event.getSymbols()).collect(Collectors.toList())),
                event.getStart(),
                event.getEnd()
        );
        GetHistoricalPricesResponseDto response = _historicalPriceMapper.mapToDto(prices);
        logger.debug("Sending {} historical price ticks matching range [{}-{}] and pairs [{}]", response.getPricesCount(), event.getStart(), event.getEnd(), Arrays.toString(event.getSymbols()));
        _connectionBroker.sendRpcResponse(
                event,
                response
        );
    }

    @ObserveEvent(eventClass = GetStartingPricesEvent.class)
    public void onGetStartingPricesEvent(GetStartingPricesEvent event) {
        logger.debug("Getting starting prices from {}", event.getStart());
        HashSet<String> allPairs = Arrays.stream(_referenceData.currencyPairs())
                .map(cp->cp.getSymbol())
                .collect(Collectors.toCollection(HashSet<String>::new));
        Collection<FxSpotPriceEvent> prices = _priceRepository.getStartPrices(
                allPairs,
                event.getStart()
        );
        logger.debug("Sending get starting prices response. Pair count: {}", prices.size());
        GetStartingPricesResponseDto response = _historicalPriceMapper.mapToGetStartingPricesResponseDto(prices);
        _connectionBroker.sendRpcResponse(
                event,
                response
        );
    }
}
