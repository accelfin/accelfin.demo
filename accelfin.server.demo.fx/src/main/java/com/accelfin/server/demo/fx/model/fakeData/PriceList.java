package com.accelfin.server.demo.fx.model.fakeData;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;

public class PriceList implements Iterable<BidAndAsk> {

    private ArrayList<BidAndAsk> _prices = new ArrayList<>();

    public void add(BigDecimal bid, BigDecimal ask) {
        _prices.add(new BidAndAsk(bid, ask));
    }

    @Override
    public Iterator<BidAndAsk> iterator() {
        return new PriceIterator();
    }

    private class PriceIterator implements Iterator<BidAndAsk> {
        private int _currentIndex = 0;
        private boolean _ascend = true;

        public PriceIterator() {
        }

        @Override
        public boolean hasNext() {
            return true;
        }

        @Override
        public BidAndAsk next() {
            BidAndAsk result = _prices.get(_currentIndex);
            if(_currentIndex + 1 >= _prices.size()) {
                _ascend = false;
            } else if(_currentIndex -1 < 0) {
                _ascend = true;
            }
            _currentIndex = _ascend
                    ? _currentIndex + 1
                    : _currentIndex - 1;
            return result;
        }
    }
}
