package com.accelfin.server.demo.fx.dtos.mappers;

import com.accelfin.server.demo.fx.model.common.BlotterUpdateType;
import com.accelfin.server.demo.fx.model.common.TradeBase;
import com.google.protobuf.Message;

import java.util.List;

public interface BlotterMapper<TTrade extends TradeBase> {
    Message mapToSubscriptionResponseDto(boolean success);
    Message mapToResponseDto(BlotterUpdateType updateType, List<TTrade> trades);
}