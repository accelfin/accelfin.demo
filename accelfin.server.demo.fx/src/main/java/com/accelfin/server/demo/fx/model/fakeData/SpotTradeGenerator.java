package com.accelfin.server.demo.fx.model.fakeData;

import com.accelfin.server.Clock;
import com.esp.disposables.DisposableBase;
import com.accelfin.server.messaging.events.ClockTickEvent;
import com.accelfin.server.demo.common.model.CurrencyPair;
import com.accelfin.server.demo.common.model.Side;
import com.accelfin.server.demo.fx.model.FxService;
import com.accelfin.server.demo.fx.model.cash.blotter.SpotTrade;
import com.accelfin.server.demo.fx.model.cash.pricing.PriceRepository;
import com.accelfin.server.demo.fx.model.events.FxSpotPriceEvent;
import com.accelfin.server.demo.fx.model.events.SpotTradeExecutedEvent;
import com.esp.ObserveEvent;
import com.esp.Router;

import java.math.BigDecimal;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.UUID;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;

public class SpotTradeGenerator extends DisposableBase {
    private Router<FxService> _router;
    private Clock _clock;
    private PriceRepository _priceRepository;
    private SpotTrade _lastTrade;
    private SpotTrade _lastAlertTrade;
    private int _tradeGeneratedCount;
    private int _alertTradeGeneratedCount;

    SpotTradeGenerator(
            Router<FxService> router,
            Clock clock,
            PriceRepository priceRepository
    ) {
        _router = router;
        _clock = clock;
        _priceRepository = priceRepository;
    }

    public void observeEvents() {
        addDisposable(_router.observeEventsOn(this));
    }

    @ObserveEvent(eventClass = ClockTickEvent.class)
    public void onClockTickEvent() {
        SpotTrade nextTrade = getNextTrade(_clock.now());
        if(nextTrade != null) {
            _router.publishEvent(new SpotTradeExecutedEvent(nextTrade));
        }
    }

    public SpotTrade getNextTrade(Instant now) {
        long TRADE_BOOKING_INTERVAL_MS = TimeUnit.MILLISECONDS.convert(2, TimeUnit.MINUTES);
        boolean shouldGenerate = _lastTrade == null || now.isAfter(_lastTrade.getSubmittedAt().plus(TRADE_BOOKING_INTERVAL_MS, ChronoUnit.MILLIS));
        if(!shouldGenerate) {
            return null;
        }
        long ALERT_TRADE_BOOKING_UPPER_BOUND_INTERVAL_MS = TimeUnit.MILLISECONDS.convert(20, TimeUnit.MINUTES);
        boolean isAlertTrade = _lastAlertTrade == null || now.isAfter(_lastAlertTrade.getSubmittedAt().plus(ALERT_TRADE_BOOKING_UPPER_BOUND_INTERVAL_MS, ChronoUnit.MILLIS));
        ThreadLocalRandom random = ThreadLocalRandom.current();
        CurrencyPair[] pairs = CurrencyPairsMetadata.Instance.getPairs();
        CurrencyPair currencyPair = pairs[random.nextInt(0, pairs.length - 1)];
        FxSpotPriceEvent price = _priceRepository.getLastPrice(currencyPair.getSymbol());
        BigDecimal notional = BigDecimal.valueOf(ThreadLocalRandom.current().nextInt(10000, 2000000) / 1000 * 1000);
        String tradeId = UUID.randomUUID().toString();
        Side side = now.getEpochSecond() % 2 == 0
                ? Side.Buy
                : Side.Sell;
        BigDecimal rate = side == Side.Buy
                ? price.getAsk()
                : price.getBid();
        if (isAlertTrade) {
            rate = price.getMid(); // not much of an alert
        }
        SpotTrade trade = new SpotTrade(
                tradeId,
                currencyPair,
                notional,
                side,
                price.getId(),
                rate,
                now,
                FakeUsers.Instance.getRandomUser()
        );
        _tradeGeneratedCount++;
        _lastTrade = trade;
        if (isAlertTrade) {
            _alertTradeGeneratedCount++;
            _lastAlertTrade = trade;
        }
        return trade;
    }

    public String getSummary() {
        return String.format("%s fakes trades (of which %s were alerts)", _tradeGeneratedCount, _alertTradeGeneratedCount);
    }
}
