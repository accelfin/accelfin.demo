package com.accelfin.server.demo.fx.model.orders.execution;

import com.accelfin.server.demo.common.model.Side;

public interface OrderRepositoryFactory {
    OrderRepository create(Side side);
}
