package com.accelfin.server.demo.fx.model.orders.execution;

public enum OrderBookRungType {
    BUY,
    SELL,
    MID
}
