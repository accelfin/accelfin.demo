package com.accelfin.server.demo.fx.model.cash.execution;

import com.accelfin.server.Clock;
import com.accelfin.server.demo.common.messaging.ConnectionBroker;
import com.accelfin.server.messaging.events.InitEvent;
import com.accelfin.server.demo.FxOperationNameConst;
import com.accelfin.server.demo.fx.dtos.ExecuteSpotResponseDto;
import com.accelfin.server.demo.fx.dtos.mappers.CashExecutionMapper;
import com.accelfin.server.demo.fx.model.FxService;
import com.accelfin.server.demo.common.model.Side;
import com.accelfin.server.demo.fx.model.User;
import com.accelfin.server.demo.fx.model.cash.blotter.SpotTrade;
import com.accelfin.server.demo.fx.model.events.SpotTradeExecutedEvent;
import com.accelfin.server.demo.fx.model.events.operationEvents.ExecuteSpotOperationEvent;
import com.accelfin.server.demo.fx.model.events.FxSpotPriceEvent;
import com.accelfin.server.demo.fx.model.cash.pricing.PriceRepository;
import com.accelfin.server.demo.fx.model.session.Sessions;
import com.accelfin.server.demo.fx.model.staticData.ReferenceData;
import com.esp.ObserveEvent;
import com.esp.Router;
import com.esp.disposables.DisposableBase;
import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Objects;
import java.util.UUID;

public class CashExecution extends DisposableBase {

    private static Logger logger = LoggerFactory.getLogger(CashExecution.class);

    private Router<FxService> _router;
    private ConnectionBroker _connectionBroker;
    private ReferenceData _referenceData;
    private CashExecutionMapper _cashExecutionMapper;
    private PriceRepository _priceRepository;
    private Clock _clock;
    private Sessions _sessions;

    @Inject
    public CashExecution(
            Router<FxService> router,
            ConnectionBroker connectionBroker,
            ReferenceData referenceData,
            CashExecutionMapper cashExecutionMapper,
            PriceRepository priceRepository,
            Clock clock,
            Sessions sessions
    ) {
        _router = router;
        _connectionBroker = connectionBroker;
        _referenceData = referenceData;
        _cashExecutionMapper = cashExecutionMapper;
        _priceRepository = priceRepository;
        _clock = clock;
        _sessions = sessions;
    }

    public void observeEvents() {
        addDisposable(_router.observeEventsOn(this));
    }

    @ObserveEvent(eventClass = InitEvent.class)
    public void onInitEvent() {
        _connectionBroker.setOperationStatus(FxOperationNameConst.ExecuteSpot, true);
    }

    @ObserveEvent(eventClass = ExecuteSpotOperationEvent.class)
    public void onExecuteSpotOperationEvent(ExecuteSpotOperationEvent event) {
        logger.debug("Spot execution event received");

        ExecuteSpotResponseDto responseDto;
        User user = _sessions.getUserForSession(event.getSessionToken());

        FxSpotPriceEvent price = _priceRepository.getPrice(event.getPriceId());
        if (price == FxSpotPriceEvent.None) {
            String errorMessage = "Price expired";
            responseDto = _cashExecutionMapper.mapExecuteSpotFailure(errorMessage);
        } else if (Objects.equals(event.getSymbol(), "USDJPY")) {
            // for the demo, we'll use USD JPY as the failure condition
            String errorMessage = "Credit limit reached";
            responseDto = _cashExecutionMapper.mapExecuteSpotFailure(errorMessage);
        } else {
            responseDto = _cashExecutionMapper.mapExecuteSpotSuccess();
            SpotTrade trade = new SpotTrade(
                    UUID.randomUUID().toString(),
                    _referenceData.getCurrencyPair(event.getSymbol()),
                    event.getAmount(),
                    event.getSide() == Side.Buy
                            ? Side.Buy
                            : Side.Sell,
                    price.getId(),
                    event.getPrice(), // might differ from actual price, alerts will pick it up, TODO in reality we'd not do this, we'd do a last look
                    _clock.now(),
                    user
            );
            SpotTradeExecutedEvent executedEvent = new SpotTradeExecutedEvent(trade);
            _router.publishEvent(executedEvent);
        }

        _connectionBroker.sendRpcResponse(event, responseDto);
    }
}
