package com.accelfin.server.demo.fx.dtos.mappers;

import com.accelfin.server.messaging.dtos.TimestampDto;
import com.accelfin.server.messaging.MessageEnvelope;
import com.accelfin.server.messaging.events.InboundMessageEvent;
import com.accelfin.server.demo.fx.dtos.*;
import com.accelfin.server.demo.fx.model.events.operationEvents.GetHistoricalPricesEvent;
import com.accelfin.server.demo.fx.model.events.operationEvents.GetStartingPricesEvent;
import com.accelfin.server.demo.fx.model.events.FxSpotPriceEvent;
import com.accelfin.server.demo.fx.model.cash.pricing.InterpolationSettings;
import com.accelfin.server.demo.fx.model.cash.pricing.TemporalUnit;

import java.time.Instant;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import static com.accelfin.server.messaging.mappers.CommonTypesMapper.mapBigDecimalToDto;

public class HistoricalPriceMapper {

    public InboundMessageEvent mapGetStartPricesEvent(MessageEnvelope envelope) {
        GetStartingPricesRequestDto dto = envelope.getPayload();
        return new GetStartingPricesEvent(
                Instant.ofEpochSecond(dto.getStart().getSeconds())
        );
    }

    public InboundMessageEvent mapGetHistoricalPricesEvent(MessageEnvelope envelope) {
        GetHistoricalPricesRequestDto dto = envelope.getPayload();
        return new GetHistoricalPricesEvent(
                dto.getSymbolsList().toArray(new String[dto.getSymbolsCount()]),
                Instant.ofEpochSecond(dto.getStart().getSeconds()),
                Instant.ofEpochSecond(dto.getEnd().getSeconds()),
                mapInterpolationSettings(dto.getInterpolationSettings())
        );
    }

    public GetHistoricalPricesResponseDto mapToDto(Collection<FxSpotPriceEvent> prices) {
        List<FxHistoricalPriceDto> priceDtos = prices
                .stream().map(this::mapToDto)
                .collect(Collectors.toList());
        return GetHistoricalPricesResponseDto.newBuilder().addAllPrices(priceDtos).build();
    }

    public FxHistoricalPriceDto mapToDto(FxSpotPriceEvent price) {
        return  FxHistoricalPriceDto.newBuilder()
                .setId(price.getId())
                .setSymbol(price.getSymbol())
                .setSpotDate(TimestampDto.newBuilder().setSeconds(price.getSpotDate().getEpochSecond()))
                .setMid(mapBigDecimalToDto(price.getMid()))
                .setAsk(mapBigDecimalToDto((price.getAsk())))
                .setBid(mapBigDecimalToDto((price.getBid())))
                .setSpread(mapBigDecimalToDto((price.getSpread())))
                .setCreationDate(TimestampDto.newBuilder().setSeconds(price.getCreationDate().getEpochSecond()))
                .build();
    }

    public GetStartingPricesResponseDto mapToGetStartingPricesResponseDto(Collection<FxSpotPriceEvent> prices) {
        List<FxHistoricalPriceDto> priceDtos = prices
                .stream().map(this::mapToDto)
                .collect(Collectors.toList());
        return GetStartingPricesResponseDto.newBuilder().addAllStartPrices(priceDtos).build();
    }

    private InterpolationSettings mapInterpolationSettings(InterpolationSettingsDto dto) {
        TemporalUnitDto temporalUnit = dto.getTemporalUnit();
        return new InterpolationSettings(
                dto.getMaxPointsPerUnit(),
                temporalUnit == TemporalUnitDto.HOUR ? TemporalUnit.Hour : TemporalUnit.Minute
        );
    }
}
