package com.accelfin.server.demo.fx.model.orders.execution;

import com.accelfin.server.Clock;
import com.esp.disposables.DisposableBase;
import com.accelfin.server.demo.common.model.CurrencyPair;
import com.accelfin.server.demo.common.model.Side;
import com.accelfin.server.demo.fx.model.FxService;
import com.accelfin.server.demo.fx.model.orders.blotter.OrderStatus;
import com.accelfin.server.demo.fx.model.orders.blotter.SimpleOrder;
import com.accelfin.server.demo.fx.model.events.FxSpotPriceEvent;
import com.accelfin.server.demo.fx.model.events.OrdersFilledEvent;
import com.accelfin.server.demo.fx.model.staticData.ReferenceData;
import com.esp.Router;
import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

public class OrderRepository extends DisposableBase {
    private static Logger logger = LoggerFactory.getLogger(OrderBook.class);

    private final boolean _isBuyOrderRepository;
    private Side _side;
    private Router<FxService> _router;
    private ReferenceData _referenceData;
    private Clock _clock;
    private HashMap<String, TreeMap<BigDecimal, ArrayList<SimpleOrder>>> _ordersByExecuteRateByPair = new HashMap<>();
    private OrderBookRungType _rungType;

    @Inject
    public OrderRepository(@Assisted Side side, Router<FxService> router, ReferenceData referenceData, Clock clock) {
        _side = side;
        _isBuyOrderRepository = _side.equals(Side.Buy);
        _rungType = side == Side.Buy ? OrderBookRungType.BUY : OrderBookRungType.SELL;
        _router = router;
        _referenceData = referenceData;
        _clock = clock;
    }

    public void initialiseWithPairs(CurrencyPair[] currencyPairs) {
        for (CurrencyPair pair : currencyPairs) {
            _ordersByExecuteRateByPair.put(pair.getSymbol(), new TreeMap<>());
        }
    }

    public void tryFillOrders(FxSpotPriceEvent event) {
        BigDecimal rate = _isBuyOrderRepository ? event.getAsk() : event.getBid();
        TreeMap<BigDecimal, ArrayList<SimpleOrder>> ordersForPair = _ordersByExecuteRateByPair.get(event.getSymbol());
        // The order execution rates are stored from low to high.
        // We want to get everything from the rate to the head/tail of the list depending on the side.
        SortedMap<BigDecimal, ArrayList<SimpleOrder>> ordersByExecuteRate = _isBuyOrderRepository
                ? ordersForPair.tailMap(rate, true)
                : ordersForPair.headMap(rate, true);
        if (ordersByExecuteRate.size() > 0) {
            List<BigDecimal> keys = Collections.list(Collections.enumeration(ordersByExecuteRate.keySet()));
            List<SimpleOrder> orders = ordersByExecuteRate
                    .values()
                    .stream()
                    .flatMap(Collection::stream)
                    .collect(Collectors.toList());
            orders.forEach(simpleOrder -> {
                simpleOrder.setStatus(OrderStatus.EXECUTED);
                simpleOrder.setExecutedRate(rate);
                simpleOrder.setExecutedAt(_clock.now());
                logOrderAction(simpleOrder, "EXECUTED");
            });
            OrdersFilledEvent ordersFilledEvent = new OrdersFilledEvent(orders);
            _router.publishEvent(ordersFilledEvent);
            ordersByExecuteRate.keySet().removeAll(keys);
        }
    }

    public void add(SimpleOrder order) {
        String symbol = order.getCurrencyPair().getSymbol();
        if (!order.getSide().equals(_side)) {
            throw new RuntimeException(String.format("Can't add [%s] order to [%s] repository", order.getSide(), _side));
        }
        TreeMap<BigDecimal, ArrayList<SimpleOrder>> ordersByExecuteRate = _ordersByExecuteRateByPair.get(symbol);
        BigDecimal submittedRate = order.getSubmittedRate();
        ArrayList<SimpleOrder> orders = ordersByExecuteRate.get(submittedRate);
        if (orders == null) {
            orders = new ArrayList<>();
            ordersByExecuteRate.put(submittedRate, orders);
        }
        logOrderAction(order, "ADDED");
        orders.add(order);
    }

    // check rev df4d03acaba2a899f276201688a5a8a70e5aaaf8 for a version that attempted bucketing, but badly
    // ultimately I think this needs to bucket rungs in larger numbers, not just pips.
    // to do this we should scale up the rates and select a subMap of all rates (similarly to how we execute)
    // then compute the notional for that larger bucket
    public List<OrderBookRung> getOrderBookRungs(int numberOfRungs, FxSpotPriceEvent midPrice) {
        TreeMap<BigDecimal, ArrayList<SimpleOrder>> ordersByExecuteRate = _ordersByExecuteRateByPair.get(midPrice.getSymbol());
        CurrencyPair currencyPair = _referenceData.getCurrencyPair(midPrice.getSymbol());
        BigDecimal rateFraction = BigDecimal.valueOf(1).divide(
                BigDecimal.valueOf(10).pow(currencyPair.getRatePrecision())
        );  // i.e. rateFraction would be 0.00001 if precision is 5

        // figure out a starting point rate.
        // note we maintain an descending order for the rungs regardless of side,
        // Given sell always appears at the top of the order book we take that into consideration in our starting 'rate' value
        BigDecimal rate = _isBuyOrderRepository
                ? midPrice.getMid().add(rateFraction.multiply(BigDecimal.valueOf(numberOfRungs)))
                : midPrice.getMid().subtract(rateFraction);
        List<OrderBookRung> rungs = new ArrayList<>(numberOfRungs);
        int count = 0;
        do {
            ArrayList<SimpleOrder> orders = ordersByExecuteRate.get(rate);
            BigDecimal total;
            if (orders != null) {
                total = orders.stream()
                        .map(SimpleOrder::getNotional)
                        .reduce(BigDecimal.ZERO, BigDecimal::add);
            } else {
                total = BigDecimal.ZERO;
            }
            OrderBookRung rung = new OrderBookRung(total, rate, _rungType);
            rungs.add(rung);
            rate = rate.subtract(rateFraction);
            count++;
        } while (count < numberOfRungs);
        return rungs;
    }

    private void logOrderAction(SimpleOrder order, String action) {
        logger.info("[{}-Order] {}:\n{}", _side, action, order.toString());
    }
}
