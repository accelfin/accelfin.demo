package com.accelfin.server.demo.fx.model.events.operationEvents;

import com.accelfin.server.AuthenticationStatus;
import com.accelfin.server.messaging.events.InboundMessageEvent;
import com.accelfin.server.demo.common.model.Side;
import com.accelfin.server.demo.fx.model.orders.blotter.OrderType;

import java.math.BigDecimal;

public class ExecuteSimpleOrderOperationEvent extends InboundMessageEvent {

    private Side _side;
    private OrderType _type;
    private BigDecimal _notional;
    private BigDecimal _submittedRate;
    private String _currencyPair;

    public ExecuteSimpleOrderOperationEvent(
            Side side,
            OrderType type,
            BigDecimal notional,
            BigDecimal submittedRate,
            String currencyPair
    ) {
        _side = side;
        _type = type;
        _notional = notional;
        _submittedRate = submittedRate;
        _currencyPair = currencyPair;
    }

    @Override
    public AuthenticationStatus getRequiredAuthenticationStatus() {
        return AuthenticationStatus.Authenticated;
    }

    public Side getSide() {
        return _side;
    }

    public OrderType getType() {
        return _type;
    }

    public BigDecimal getNotional() {
        return _notional;
    }

    public BigDecimal getSubmittedRate() {
        return _submittedRate;
    }

    public String getCurrencyPair() {
        return _currencyPair;
    }
}