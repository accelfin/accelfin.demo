package com.accelfin.server.demo.fx.model;

import com.accelfin.server.Clock;
import com.accelfin.server.ServiceModelBase;
import com.accelfin.server.demo.common.messaging.ConnectionBroker;
import com.accelfin.server.messaging.Connections;
import com.accelfin.server.messaging.events.InitEvent;
import com.accelfin.server.demo.fx.model.cash.blotter.CashBlotter;
import com.accelfin.server.demo.fx.model.events.CurrencyPairsResolvedEvent;
import com.accelfin.server.demo.fx.model.fakeData.FakeActivities;
import com.accelfin.server.demo.fx.model.orders.blotter.OrderBlotter;
import com.accelfin.server.demo.fx.model.cash.execution.CashExecution;
import com.accelfin.server.demo.fx.model.orders.execution.OrderExecution;
import com.accelfin.server.demo.fx.model.orders.execution.OrderBook;
import com.accelfin.server.demo.fx.model.cash.pricing.Pricer;
import com.accelfin.server.demo.fx.model.session.Sessions;
import com.accelfin.server.demo.fx.model.staticData.ReferenceData;
import com.esp.ObservationStage;
import com.esp.ObserveEvent;
import com.esp.Router;
import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FxService extends ServiceModelBase {
    private static Logger logger = LoggerFactory.getLogger(FxService.class);

    private final Pricer _pricer;
    private CashBlotter _cashBlotter;
    private OrderBlotter _orderBlotter;
    private final Sessions _sessions;
    private final ReferenceData _referenceData;
    private CashExecution _cashExecution;
    private OrderExecution _orderExecution;
    private FakeActivities _fakeActivities;
    private OrderBook _orderBook;

    private Router<FxService> _router;

    @Inject
    public FxService(
            Router<FxService> router,
            Pricer pricer,
            CashBlotter cashBlotter,
            OrderBlotter orderBlotter,
            ReferenceData referenceData,
            Sessions sessions,
            CashExecution cashExecution,
            OrderExecution orderExecution,
            Clock clock,
            Connections connections,
            FakeActivities fakeActivities,
            OrderBook orderBook
    ) {
        super(clock, connections);
        _router = router;
        _pricer = pricer;
        _cashBlotter = cashBlotter;
        _orderBlotter = orderBlotter;
        _sessions = sessions;
        _referenceData = referenceData;
        _cashExecution = cashExecution;
        _orderExecution = orderExecution;
        _fakeActivities = fakeActivities;
        _orderBook = orderBook;
    }

    public Pricer pricer() {
        return _pricer;
    }

    public Sessions sessions() {
        return _sessions;
    }

    public ReferenceData referenceData() {
        return _referenceData;
    }

    public CashExecution fxExecution() {
        return _cashExecution;
    }

    public OrderExecution orderExecution() {
        return _orderExecution;
    }

    public OrderBlotter orderBlotter() {
        return _orderBlotter;
    }

    public CashBlotter blotter() {
        return _cashBlotter;
    }

    public OrderBook orderBook() {
        return _orderBook;
    }

    public FakeActivities getFakeActivities() {
        return _fakeActivities;
    }

    @Override
    public void observeEvents() {
        super.observeEvents();
        addDisposable(_router.observeEventsOn(this));
        _pricer.observeEvents();
        _sessions.observeEvents();
        _referenceData.observeEvents();
        _cashExecution.observeEvents();
        _cashBlotter.observeEvents();
        _orderExecution.observeEvents();
        _orderBlotter.observeEvents();
        _orderBook.observeEvents();
        _fakeActivities.observeEvents();
    }

    @ObserveEvent(eventClass = InitEvent.class)
    public void onInitEvent() {
        logger.info("Initialising ref data and starting timer");
        _referenceData.beginLoadData();
    }

    @ObserveEvent(eventClass = CurrencyPairsResolvedEvent.class, stage = ObservationStage.Committed)
    public void onCurrencyPairsResolvedEvent() {
        logger.info("Currency pairs loaded. Connecting Mq");
        getConnections().connectAll();
    }
}