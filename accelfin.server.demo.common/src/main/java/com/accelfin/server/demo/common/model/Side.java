package com.accelfin.server.demo.common.model;

public enum Side {
    Buy,
    Sell
}
