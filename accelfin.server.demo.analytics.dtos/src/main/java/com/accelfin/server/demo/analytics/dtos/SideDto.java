// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: accelfin-server-demo-analytics-dtos.proto

package com.accelfin.server.demo.analytics.dtos;

/**
 * Protobuf enum {@code com.accelfin.server.demo.analytics.dtos.SideDto}
 */
public enum SideDto
    implements com.google.protobuf.ProtocolMessageEnum {
  /**
   * <code>BUY = 0;</code>
   */
  BUY(0),
  /**
   * <code>SELL = 1;</code>
   */
  SELL(1),
  UNRECOGNIZED(-1),
  ;

  /**
   * <code>BUY = 0;</code>
   */
  public static final int BUY_VALUE = 0;
  /**
   * <code>SELL = 1;</code>
   */
  public static final int SELL_VALUE = 1;


  public final int getNumber() {
    if (this == UNRECOGNIZED) {
      throw new java.lang.IllegalArgumentException(
          "Can't get the number of an unknown enum value.");
    }
    return value;
  }

  /**
   * @deprecated Use {@link #forNumber(int)} instead.
   */
  @java.lang.Deprecated
  public static SideDto valueOf(int value) {
    return forNumber(value);
  }

  public static SideDto forNumber(int value) {
    switch (value) {
      case 0: return BUY;
      case 1: return SELL;
      default: return null;
    }
  }

  public static com.google.protobuf.Internal.EnumLiteMap<SideDto>
      internalGetValueMap() {
    return internalValueMap;
  }
  private static final com.google.protobuf.Internal.EnumLiteMap<
      SideDto> internalValueMap =
        new com.google.protobuf.Internal.EnumLiteMap<SideDto>() {
          public SideDto findValueByNumber(int number) {
            return SideDto.forNumber(number);
          }
        };

  public final com.google.protobuf.Descriptors.EnumValueDescriptor
      getValueDescriptor() {
    return getDescriptor().getValues().get(ordinal());
  }
  public final com.google.protobuf.Descriptors.EnumDescriptor
      getDescriptorForType() {
    return getDescriptor();
  }
  public static final com.google.protobuf.Descriptors.EnumDescriptor
      getDescriptor() {
    return com.accelfin.server.demo.analytics.dtos.AccelfinServerDemoAnalyticsDtos.getDescriptor()
        .getEnumTypes().get(1);
  }

  private static final SideDto[] VALUES = values();

  public static SideDto valueOf(
      com.google.protobuf.Descriptors.EnumValueDescriptor desc) {
    if (desc.getType() != getDescriptor()) {
      throw new java.lang.IllegalArgumentException(
        "EnumValueDescriptor is not for this type.");
    }
    if (desc.getIndex() == -1) {
      return UNRECOGNIZED;
    }
    return VALUES[desc.getIndex()];
  }

  private final int value;

  private SideDto(int value) {
    this.value = value;
  }

  // @@protoc_insertion_point(enum_scope:com.accelfin.server.demo.analytics.dtos.SideDto)
}

