package com.accelfin.server.demo.analytics.model.reports;

import com.esp.disposables.DisposableBase;
import com.accelfin.server.demo.common.model.MathUtils;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Map;


public class FxAlertReportUpdateUnitOfWork extends DisposableBase {

    private static final int MAX_POINTS_IN_MAIN_VIEW = 500;
    private static final int MAX_POINTS_IN_RANGE_VIEW = 500;

    private String _correlationId;
    private Instant _rangeViewStart;
    private Instant _rangeViewEnd;
    private Instant _mainViewStart;
    private Instant _mainViewEnd;
    private ArrayList<String> _subscribedSymbols;
    private boolean _willFetchPrices;
    private boolean _startingPricesReceived;
    private boolean _isPricesReceived;
    private boolean _isTradesReceived;
    // we default the _allPrices to an empty, in the case where we don't receive _allPrices this just makes things easier
    private ArrayList<FxHistoricalSpotPrice> _allPrices = new ArrayList<>();
    private ArrayList<FxHistoricalSpotPrice> _mainViewPrices = new ArrayList<>();
    private ArrayList<FxHistoricalSpotPrice> _rangeViewPrices = new ArrayList<>();
    private ArrayList<FxHistoricalTrade> _trades;
    private Map<String, FxHistoricalSpotPrice> _startPricesBySymbol;
    private boolean _hasCompleted = false;

    public FxAlertReportUpdateUnitOfWork(
            String correlationId,
            Instant rangeViewStart,
            Instant rangeViewEnd,
            Instant mainViewStart,
            Instant mainViewEnd,
            ArrayList<String> subscribedSymbols
    ) {
        _correlationId = correlationId;
        _rangeViewStart = rangeViewStart;
        _rangeViewEnd = rangeViewEnd;
        _mainViewStart = mainViewStart;
        _mainViewEnd = mainViewEnd;
        _subscribedSymbols = subscribedSymbols;
        _willFetchPrices = subscribedSymbols.size() > 0;
    }

    public String getCorrelationId() {
        return _correlationId;
    }

    public Instant getMainViewStart() {
        return _mainViewStart;
    }

    public Instant getRangeViewEnd() {
        return _rangeViewEnd;
    }

    public Instant getMainViewEnd() {
        return _mainViewEnd;
    }

    public Instant getRangeViewStart() {
        return _rangeViewStart;
    }

    public ArrayList<String> getSubscribedSymbols() {
        return _subscribedSymbols;
    }

    public void setIStartingPricesReceived(boolean startingPricesReceived) {
        _startingPricesReceived = startingPricesReceived;
    }

    public void setIsPricesReceived(boolean pricesReceived) {
        _isPricesReceived = pricesReceived;
    }

    public void setIsTradesReceived(boolean tradesReceived) {
        _isTradesReceived = tradesReceived;
    }

    public ArrayList<FxHistoricalTrade> getTrades() {
        return _trades;
    }

    public void setTrades(ArrayList<FxHistoricalTrade> trades) {
        _trades = trades;
    }

    public void setStartPricesBySymbol(Map<String, FxHistoricalSpotPrice> value) {
        _startPricesBySymbol = value;
    }

    public ArrayList<FxHistoricalSpotPrice> getAllPrices() {
        return _allPrices;
    }

    public void setAllPrices(ArrayList<FxHistoricalSpotPrice> allPrices) {
        _allPrices = allPrices;
    }

    public boolean getHasCompleted() {
        return _hasCompleted;
    }

    public boolean getWillFetchPrices() {
        return _willFetchPrices;
    }

    public ArrayList<FxHistoricalSpotPrice> getRangeViewPrices() {
        return _rangeViewPrices;
    }

    public ArrayList<FxHistoricalSpotPrice> getMainViewPrices() {
        return _mainViewPrices;
    }

    public void tryComplete() {
        if ((_isPricesReceived || !_willFetchPrices) && _startingPricesReceived && _isTradesReceived && !isDisposed()) {
            convertPriceRatesToPercentages();
            scanTradesForAlerts(); // do this before we convert trade rates to %
            convertTradeRatesToPercentages();
            ScanResults sampledPriceLists = createSampledPriceLists(_mainViewStart, _mainViewEnd, _allPrices);
            _mainViewPrices = sampledPriceLists.mainViewPrices;
            _rangeViewPrices = sampledPriceLists.rangeViewPrices;
            _hasCompleted = true;
            this.dispose();
        }
    }

    private void convertPriceRatesToPercentages() {
        ArrayList<FxHistoricalSpotPrice> newList = new ArrayList<>();
        for (FxHistoricalSpotPrice price : _allPrices) {
            FxHistoricalSpotPrice startingPrice = _startPricesBySymbol.get(price.getSymbol());
            newList.add(price.convertToPercentages(startingPrice));
        }
        _allPrices = newList;
    }

    private void scanTradesForAlerts() {
        for (FxHistoricalTrade trade : _trades) {
            if (trade.getExecutedRate().equals(trade.getSpotPrice().getMid())) {
                trade.addAlertCode(AlertCodes.ZeroOrNegativeSpread);
            }
        }
    }

    private void convertTradeRatesToPercentages() {
        for (FxHistoricalTrade trade : _trades) {
            FxHistoricalSpotPrice startingPrice = _startPricesBySymbol.get(trade.getCurrencyPair());
            // Seems reasonable to using the starting mid to get the execution rate percentage.
            // The trade does have the initial price, however it also has executed rate which is what we need
            BigDecimal midChangePercentage = MathUtils.getChangePercent(
                    startingPrice.getMid(),
                    trade.getExecutedRate()
            );
            trade.setNormalisedRate(midChangePercentage);
        }
    }

    private ScanResults createSampledPriceLists(Instant mainViewStart, Instant mainViewEnd, ArrayList<FxHistoricalSpotPrice> prices) {
        ArrayList<FxHistoricalSpotPrice> sampledRangeView = new ArrayList<>();
        ArrayList<FxHistoricalSpotPrice> sampledMainView = new ArrayList<>();
        int mainViewStartIndex = -1;
        int mainViewEndIndex = -1;
        int numberOfPointsInMainView = 0;
        if (prices.size() > MAX_POINTS_IN_RANGE_VIEW) {
            int rangeViewSampleFrequency = prices.size() / MAX_POINTS_IN_RANGE_VIEW;
            for (int i = 0; i < prices.size(); i++) {
                FxHistoricalSpotPrice price = prices.get(i);
                if (i % rangeViewSampleFrequency == 0) {
                    sampledRangeView.add(price);
                }
                if (price.getCreationDate().isAfter(mainViewStart) && mainViewStartIndex == -1) {
                    mainViewStartIndex = i;
                }
                if (price.getCreationDate().isAfter(mainViewEnd) && mainViewEndIndex == -1) {
                    mainViewEndIndex = i;
                }
            }
            if (mainViewStartIndex == -1) mainViewStartIndex = 0;
            if (mainViewEndIndex == -1) mainViewEndIndex = prices.size();
            numberOfPointsInMainView = mainViewEndIndex - mainViewStartIndex;
            if (numberOfPointsInMainView > MAX_POINTS_IN_MAIN_VIEW) {
                int mainViewSampleFrequency = numberOfPointsInMainView / MAX_POINTS_IN_MAIN_VIEW;
                for (int i = mainViewStartIndex; i < mainViewEndIndex; i++) {
                    FxHistoricalSpotPrice price = prices.get(i);
                    if (i % mainViewSampleFrequency == 0) {
                        sampledMainView.add(price);
                    }
                }
            } else {
                sampledMainView.addAll(
                        prices.subList(mainViewStartIndex, mainViewEndIndex)
                );
            }
        } else {
            sampledRangeView.addAll(prices);
            sampledMainView.addAll(prices);
        }
        return new ScanResults(sampledMainView, sampledRangeView);
    }

    private static class ScanResults {
        public ScanResults(ArrayList<FxHistoricalSpotPrice> mainViewPrices, ArrayList<FxHistoricalSpotPrice> rangeViewPrices) {
            this.mainViewPrices = mainViewPrices;
            this.rangeViewPrices = rangeViewPrices;
        }

        private ArrayList<FxHistoricalSpotPrice> mainViewPrices;
        private ArrayList<FxHistoricalSpotPrice> rangeViewPrices;
    }

    @Override
    public String toString() {
        return "FxAlertReportUpdateUnitOfWork{" +
                "_correlationId='" + _correlationId + '\'' +
                ", _rangeViewStart=" + _rangeViewStart +
                ", _rangeViewEnd=" + _rangeViewEnd +
                ", _mainViewStart=" + _mainViewStart +
                ", _mainViewEnd=" + _mainViewEnd +
                ", _subscribedSymbols=" + _subscribedSymbols +
                '}';
    }
}


