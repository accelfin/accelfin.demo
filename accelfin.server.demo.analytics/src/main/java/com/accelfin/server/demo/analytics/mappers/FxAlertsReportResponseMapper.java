package com.accelfin.server.demo.analytics.mappers;

import com.accelfin.server.demo.analytics.dtos.FxAlertsReportResponseDto;
import com.accelfin.server.demo.analytics.dtos.FxAlertsReportSymbolPriceDto;
import com.accelfin.server.demo.analytics.dtos.FxAlertsReportTradeDto;
import com.accelfin.server.demo.analytics.dtos.SideDto;
import com.accelfin.server.demo.analytics.model.reports.FxAlertsReport;
import com.accelfin.server.demo.analytics.model.reports.FxHistoricalSpotPrice;
import com.accelfin.server.demo.analytics.model.reports.FxHistoricalTrade;
import com.accelfin.server.demo.common.model.Side;

import java.util.*;
import java.util.stream.Collectors;

import static com.accelfin.server.messaging.mappers.CommonTypesMapper.mapBigDecimalToDto;
import static com.accelfin.server.messaging.mappers.CommonTypesMapper.mapInstantToDto;

public class FxAlertsReportResponseMapper {
    public FxAlertsReportResponseDto mapToDto(FxAlertsReport report) {
        return FxAlertsReportResponseDto.newBuilder()
                .setReportId(report.getReportId())
                .addAllRangeViewPrices(mapPrices(report.getRangeViewPrices()))
                .addAllMainViewPrices(mapPrices(report.getMainViewPrices()))
                .addAllMainViewTrades(mapTrades(report.getMainViewTrades()))
                .build();
    }

    private List<FxAlertsReportSymbolPriceDto> mapPrices(Collection<FxHistoricalSpotPrice> prices) {
        return prices
                .stream()
                .map(price -> FxAlertsReportSymbolPriceDto.newBuilder()
                        .setTime(mapInstantToDto(price.getCreationDate()))
                        .setSymbol(price.getSymbol())
                        .setMid(mapBigDecimalToDto(price.getMid()))
                        .setAsk(mapBigDecimalToDto(price.getAsk()))
                        .setBid(mapBigDecimalToDto(price.getBid()))
                        .build()
                ).collect(Collectors.toList());
    }

    private Iterable<FxAlertsReportTradeDto> mapTrades(Collection<FxHistoricalTrade> trades) {
        return trades
                .stream()
                .map(trade ->
                        FxAlertsReportTradeDto.newBuilder()
                                .setTradeId(trade.getTradeId())
                                .setCurrencyPair(trade.getCurrencyPair())
                                .setNotional(mapBigDecimalToDto(trade.getNotional()))
                                .setSide(trade.getSide() == Side.Buy ? SideDto.BUY : SideDto.SELL)
                                .setNormalisedRate(mapBigDecimalToDto(trade.getNormalisedRate()))
                                .setRate(mapBigDecimalToDto(trade.getExecutedRate()))
                                .setExecutedAt(mapInstantToDto(trade.getExecutedAtTime()))
                                .setHasAlert(trade.getHasAlert())
                                .addAllAlertCodes(trade.getAlertCodes())
                            .build()

                ).collect(Collectors.toList());
    }
}
