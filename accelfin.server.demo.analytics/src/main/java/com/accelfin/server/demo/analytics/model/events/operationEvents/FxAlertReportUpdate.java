package com.accelfin.server.demo.analytics.model.events.operationEvents;

import com.accelfin.server.demo.analytics.model.reports.ReportType;

import java.time.Instant;

public class FxAlertReportUpdate extends ReportBase {
    private String[] _symbols;
    private Instant _rangeViewStart;
    private Instant _rangeViewEnd;
    private Instant _mainViewStart;
    private Instant _mainViewEnd;

    public FxAlertReportUpdate(String reportId, String[] symbols, Instant rangeViewStart, Instant rangeViewEnd, Instant mainViewStart, Instant mainViewEnd) {
        super(reportId);
        _symbols = symbols;
        _rangeViewStart = rangeViewStart;
        _rangeViewEnd = rangeViewEnd;
        _mainViewStart = mainViewStart;
        _mainViewEnd = mainViewEnd;
    }

    @Override
    public ReportType getReportType() {
        return ReportType.FxAlerts;
    }

    public String[] getSymbols() {
        return _symbols;
    }

    public void setSymbols(String[] symbols) {
        _symbols = symbols;
    }

    public Instant getRangeViewStart() {
        return _rangeViewStart;
    }

    public void setRangeViewStart(Instant rangeViewStart) {
        _rangeViewStart = rangeViewStart;
    }

    public Instant getRangeViewEnd() {
        return _rangeViewEnd;
    }

    public void setRangeViewEnd(Instant rangeViewEnd) {
        _rangeViewEnd = rangeViewEnd;
    }

    public Instant getMainViewStart() {
        return _mainViewStart;
    }

    public void setMainViewStart(Instant mainViewStart) {
        _mainViewStart = mainViewStart;
    }

    public Instant getMainViewEnd() {
        return _mainViewEnd;
    }

    public void setMainViewEnd(Instant mainViewEnd) {
        _mainViewEnd = mainViewEnd;
    }
}
