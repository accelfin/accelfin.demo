package com.accelfin.server.demo.analytics.model.reports;

import com.accelfin.server.demo.common.messaging.ConnectionBroker;
import com.accelfin.server.messaging.events.InitEvent;
import com.accelfin.server.messaging.events.RemoteServiceConnectionStatusChangedEvent;
import com.accelfin.server.demo.analytics.model.AnalyticsService;
import com.accelfin.server.demo.analytics.model.events.operationEvents.FxAlertReportUpdate;
import com.accelfin.server.demo.analytics.model.events.operationEvents.UpdateReportSubscriptionRequestOperationEvent;
import com.accelfin.server.demo.analytics.model.events.operationEvents.ReportBase;
import com.accelfin.server.demo.AnalyicsOperationNameConst;
import com.accelfin.server.demo.ServiceTypesConst;
import com.esp.EventContext;
import com.esp.ObserveEvent;
import com.esp.Router;
import com.esp.disposables.DisposableBase;
import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Reports extends DisposableBase {

    private static Logger logger = LoggerFactory.getLogger(Reports.class);

    private final Router<AnalyticsService> _router;
    private final FxAlertsReports _fxAlertsReports;
    private ConnectionBroker _connectionBroker;

    @Inject
    public Reports(Router<AnalyticsService> router, FxAlertsReports fxAlertsReports, ConnectionBroker connectionBroker) {
        _router = router;
        _fxAlertsReports = fxAlertsReports;
        _connectionBroker = connectionBroker;
    }

    public void observeEvents() {
        addDisposable(_router.observeEventsOn(this));
    }

    @ObserveEvent(eventClass = InitEvent.class)
    public void onInitEvent() {
        _connectionBroker.setOperationStatus(AnalyicsOperationNameConst.ReportUpdate, true);
    }

    @ObserveEvent(eventClass = RemoteServiceConnectionStatusChangedEvent.class)
    public void onRemoteServiceConnectionStatusChangedEvent(RemoteServiceConnectionStatusChangedEvent event) {
        if(event.getServiceType().equals(ServiceTypesConst.Fx)) {
            logger.debug("{} availability changed", AnalyicsOperationNameConst.ReportSubscriptionUpdate, event.isConnected());
            _connectionBroker.setOperationStatus(
                    AnalyicsOperationNameConst.ReportSubscriptionUpdate,
                    event.isConnected()
            );
        }
    }

    @ObserveEvent(eventClass = UpdateReportSubscriptionRequestOperationEvent.class)
    public void onUpdateReportSubscriptionRequestOperationEvent(UpdateReportSubscriptionRequestOperationEvent event, EventContext context) {
        logger.debug("Reports update event received");
        for(ReportBase report : event.getReports()) {
            switch (report.getReportType()) {
                case FxAlerts:
                    _fxAlertsReports.processReportUpdate(
                            event,
                            event.getSessionToken(),
                            (FxAlertReportUpdate)report
                    );
                    break;
                 default:
                     logger.debug("Unsupported report type sent [{}]", report.getReportType());
                    break;
            }
        }
    }
}
