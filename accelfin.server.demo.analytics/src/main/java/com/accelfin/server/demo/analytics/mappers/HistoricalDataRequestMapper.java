package com.accelfin.server.demo.analytics.mappers;

import com.accelfin.server.messaging.dtos.TimestampDto;
import com.accelfin.server.demo.fx.dtos.*;

import java.time.Instant;

public class HistoricalDataRequestMapper {
    public GetHistoricalPricesRequestDto mapToHistoricalPricesRequestDto(Iterable<String> symbols, Instant start, Instant end) {
        return GetHistoricalPricesRequestDto.newBuilder()
                .addAllSymbols(symbols)
                .setStart(TimestampDto.newBuilder().setSeconds(start.getEpochSecond()))
                .setEnd(TimestampDto.newBuilder().setSeconds(end.getEpochSecond()))
                .setInterpolationSettings(InterpolationSettingsDto.newBuilder().setTemporalUnitValue(10).setTemporalUnit(TemporalUnitDto.MINUTE))
                .build();
    }
    public GetHistoricalTradesRequestDto mapToHistoricalTradesRequestDto(Instant start, Instant end) {
        return GetHistoricalTradesRequestDto.newBuilder()
                .setStart(TimestampDto.newBuilder().setSeconds(start.getEpochSecond()))
                .setEnd(TimestampDto.newBuilder().setSeconds(end.getEpochSecond()))
                .build();
    }
    public GetStartingPricesRequestDto mapGetStartingPricesRequestDto(Instant start) {
        return GetStartingPricesRequestDto.newBuilder()
                .setStart(TimestampDto.newBuilder().setSeconds(start.getEpochSecond()))
                .build();
    }
}
