package com.accelfin.server.demo.analytics.model.events.operationEvents;

import com.accelfin.server.AuthenticationStatus;
import com.accelfin.server.messaging.events.InboundMessageEvent;
import com.accelfin.server.demo.analytics.model.reports.FxHistoricalTrade;

import java.util.ArrayList;

public class GetHistoricalTradesResponseEvent extends InboundMessageEvent {

    private ArrayList<FxHistoricalTrade> _trades;

    public GetHistoricalTradesResponseEvent(ArrayList<FxHistoricalTrade> trades) {
        _trades = trades;
    }

    @Override
    public AuthenticationStatus getRequiredAuthenticationStatus() {
        return AuthenticationStatus.Unauthenticated;
    }

    public ArrayList<FxHistoricalTrade> getTrades() {
        return _trades;
    }
}

